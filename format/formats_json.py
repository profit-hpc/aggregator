class OutputSchemes:
    text = {
        "type": "dict",
        "path": "",
        "scheme": {
            "job_id": "job.id",
            "general": {
                "user_name": "job.user_name",
                "used_queue": "job.used_queue",
                "submit_time": "job.submit_time",
                "start_time": "job.start_time",
                "end_time": "job.end_time",
                "used_time": "job.run_time",
                "requested_time": "job.requested_time",
                "requested_cu": "job.requested_cu",
                "num_nodes": "job.num_nodes",
                "exit_code": "job.exit_code",
            },
            "nodes": {
                "type": "list",
                "path": "nodes",
                "scheme": {
                    "node_name": "name",
                    "cpu_time_user": "proc.cpu_time_user",
                    "cpu_time_system": "proc.cpu_time_system",
                    "cpu_time_idle": "proc.cpu_time_idle",
                    "cpu_time_iowait": "proc.cpu_time_iowait",
                    "write_bytes": "proc.write_bytes",
                    "write_count": "proc.write_count",
                    "read_bytes": "proc.read_bytes",
                    "read_count": "proc.read_count",
                    "mem_rss_max": "proc.mem_rss_max",
                    "mem_rss_avg": "proc.mem_rss_avg",
                    "mem_swap_max": "proc.mem_swap_max",
                    "mem_vms": "proc.mem_vms",
                    "cpu_usage": "proc.cpu_usage",
                    "cpu_model": "cpu_model",
                    "sockets": "sockets",
                    "cores_per_socket": "cores_per_socket",
                    "phys_threads_per_core": "phys_thr_core",
                    "virt_threads_per_core": "virt_thr_core",
                    "available_main_mem": "main_mem",
                    "num_cores": "alloc_cu",
                    "alloc_mem": "alloc_mem",
                    "ib_rcv_max": "ib_rcv_max",
                    "ib_xmit_max": "ib_xmit_max",
                    "beegfs_read_sum": "beegfs_read_sum",
                    "beegfs_write_sum": "beegfs_write_sum",
                    "gpus": {
                        "type": "list",
                        "path": "gpus",
                        "scheme": {
                            "bus": "bus",
                            "power_limit": "power_limit",
                            "mem": "mem",
                            "mem_max": "mem_max",
                            "temp_max": "temp_max",
                            "power_max": "power_max",
                            "usage_max": "usage_max",
                            "usage_avg": "usage_avg",
                            "cpu_usage_max": "cpu_usage_max",
                            "cpu_mem_rss_max": "cpu_mem_rss_max",
                            "cpu_proc_total": "cpu_proc_total",
                        }
                    }
                }
            }
        }
    }

    pdf = {
        "type": "dict",
        "path": "",
        "scheme": {
            "job": {
                "job_id": "job.id",
                "user_name": "job.user_name",
                "used_queue": "job.used_queue",
                "submit_time": "job.submit_time",
                "start_time": "job.start_time",
                "end_time": "job.end_time",
                "used_time": "job.run_time",
                "requested_time": "job.requested_time",
                "requested_cu": "job.requested_cu",
                "num_nodes": "job.num_nodes",
                "exit_code": "job.exit_code",
            },
            "nodes": {
                "type": "list",
                "path": "nodes",
                "scheme": {
                    "node_name": "name",
                    "cpu_time_user": "proc.cpu_time_user",
                    "cpu_time_system": "proc.cpu_time_system",
                    "cpu_time_idle": "proc.cpu_time_idle",
                    "cpu_time_iowait": "proc.cpu_time_iowait",
                    "write_bytes": "proc.write_bytes",
                    "write_count": "proc.write_count",
                    "read_bytes": "proc.read_bytes",
                    "read_count": "proc.read_count",
                    "mem_rss_max": "proc.mem_rss_max",
                    "mem_rss_avg": "proc.mem_rss_avg",
                    "mem_swap_max": "proc.mem_swap_max",
                    "mem_vms": "proc.mem_vms",
                    "cpu_usage": "proc.cpu_usage",
                    "cpu_model": "cpu_model",
                    "sockets": "sockets",
                    "cores_per_socket": "cores_per_socket",
                    "phys_threads_per_core": "phys_thr_core",
                    "virt_threads_per_core": "virt_thr_core",
                    "available_main_mem": "main_mem",
                    "num_cores": "alloc_cu",
                    "alloc_mem": "alloc_mem",
                    "ib_rcv_max": "ib_rcv_max",
                    "ib_xmit_max": "ib_xmit_max",
                    "beegfs_read_sum": "beegfs_read_sum",
                    "beegfs_write_sum": "beegfs_write_sum",
                    "gpus": {
                        "type": "list",
                        "path": "gpus",
                        "scheme": {
                            "bus": "bus",
                            "power_limit": "power_limit",
                            "mem": "mem",
                            "mem_max": "mem_max",
                            "temp_max": "temp_max",
                            "power_max": "power_max",
                            "usage_max": "usage_max",
                            "usage_avg": "usage_avg",
                            "cpu_usage_max": "cpu_usage_max",
                            "cpu_mem_rss_max": "cpu_mem_rss_max",
                            "cpu_proc_total": "cpu_proc_total",
                            "dynamic": {
                                "seq_mem_avg": {
                                    "type": "seq",
                                    "path": "seq_mem_avg",
                                    "scheme": "std"
                                },
                                "seq_usage_avg": {
                                    "type": "seq",
                                    "path": "seq_usage_avg",
                                    "scheme": "std"
                                },
                                "seq_temp_avg": {
                                    "type": "seq",
                                    "path": "seq_temp_avg",
                                    "scheme": "std"
                                },
                                "seq_power_avg": {
                                    "type": "seq",
                                    "path": "seq_power_avg",
                                    "scheme": "std"
                                },
                                "seq_cpu_usage_sum": {
                                    "type": "seq",
                                    "path": "seq_cpu_usage_sum",
                                    "scheme": "std"
                                },
                                "seq_cpu_mem_rss_sum": {
                                    "type": "seq",
                                    "path": "seq_cpu_mem_rss_sum",
                                    "scheme": "std"
                                },
                                "seq_cpu_proc_count": {
                                    "type": "seq",
                                    "path": "seq_cpu_proc_count",
                                    "scheme": "std"
                                },
                            }
                        }
                    },
                    "dynamic": {
                        "node_cpu_usage": {
                            "type": "seq",
                            "path": "seq_cpu_usage",
                            "scheme": "std"
                        },
                        "node_load": {
                            "type": "seq",
                            "path": "seq_load_avg",
                            "scheme": "std"
                        },
                        "node_ib_rcv_max": {
                            "type": "seq",
                            "path": "seq_ib_rcv_max",
                            "scheme": "std"
                        },
                        "node_ib_xmit_max": {
                            "type": "seq",
                            "path": "seq_ib_xmit_max",
                            "scheme": "std"
                        },
                        "node_beegfs_read_sum": {
                            "type": "seq",
                            "path": "seq_beegfs_read_sum",
                            "scheme": "std"
                        },
                        "node_beegfs_write_sum": {
                            "type": "seq",
                            "path": "seq_beegfs_write_sum",
                            "scheme": "std"
                        },
                        "proc_cpu_usage": {
                            "type": "seq",
                            "path": "proc.seq_cpu_usage",
                            "scheme": "std"
                        },
                        "proc_mem_rss_sum": {
                            "type": "seq",
                            "path": "proc.seq_mem_rss_sum",
                            "scheme": "std"
                        },
                        "proc_write_sum": {
                            "type": "seq",
                            "path": "proc.seq_write_sum",
                            "scheme": "std"
                        },
                        "proc_read_sum": {
                            "type": "seq",
                            "path": "proc.seq_read_sum",
                            "scheme": "std"
                        },
                    }
                }
            }
        }
    }
