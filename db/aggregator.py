from .influx import influxdb_wrapper

def get_aggregator(job_id, type = "text"):
    aggr = influxdb_wrapper.get_aggregator(job_id, type)

    return aggr
