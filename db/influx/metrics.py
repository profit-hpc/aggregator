from enum import Enum
from conf import config as conf

class QType(Enum):
    CUSTOM = 0
    SUM_OF_MAX = 1
    MAX_SUM_PER_INT = 2
    AVG_SUM_PER_INT = 3
    SUM_PER_NODE = 4
    AVG_OF_NODE = 5
    MAX_OF_NODE = 6
    SUM_OF_NODE = 7
    LAST_OF_NODE = 8
    PER_DEVICE = 9
    AVG_PER_DEVICE = 10
    MAX_PER_DEVICE = 11
    LAST_OF_DEVICE = 12
    AVG_PER_DEVICE_INT = 13
    MAX_PER_DEVICE_INT = 14
    AVG_PER_PROC_ON_DEVICE_INT = 15
    PROC_SUM_PER_DEVICE_INT = 16

class MType(Enum):
    INT = 1
    STR = 2
    FLT = 3

class APInames():
    NODE_NAME = "node_name"

metrics_proc = {
    'cpu_time_user':    {
        "dbname": 'cpu_time_user',
        "query": QType.SUM_OF_MAX,
        "type": MType.INT},
    'cpu_time_system':  {
        "dbname": 'cpu_time_system',
        "query": QType.SUM_OF_MAX,
        "type": MType.INT},
    'cpu_time_idle':    {
        "dbname": 'cpu_time_idle',
        "query": QType.SUM_OF_MAX,
        "type": MType.INT},
    'cpu_time_iowait':  {
        "dbname": 'cpu_time_iowait',
        "query": QType.SUM_OF_MAX,
        "type": MType.INT},
    'write_bytes':      {
        "dbname": 'write_bytes',
        "query": QType.SUM_OF_MAX,
        "type": MType.INT},
    'write_count':      {
        "dbname": 'write_count',
        "query": QType.SUM_OF_MAX,
        "type": MType.INT},
    'read_bytes':       {
        "dbname": 'read_bytes',
        "query": QType.SUM_OF_MAX,
        "type": MType.INT},
    'read_count':       {
        "dbname": 'read_count',
        "query": QType.SUM_OF_MAX,
        "type": MType.INT},
    'mem_rss_max':      {
        "dbname": 'memory_rss',
        "query": QType.MAX_SUM_PER_INT,
        "type": MType.INT},
    'mem_rss_avg':      {
        "dbname": 'memory_rss',
        "query": QType.AVG_SUM_PER_INT,
        "type": MType.INT},
    'mem_swap_max':     {
        "dbname": 'memory_swap',
        "query": QType.MAX_SUM_PER_INT,
        "type": MType.INT},
    'mem_vms':          {
        "dbname": 'memory_vms',
        "query": QType.MAX_SUM_PER_INT,
        "type": MType.INT},
    'cpu_usage':        {
        "dbname": 'cpu_usage',
        "query": QType.MAX_SUM_PER_INT,
        "type": MType.INT},
}

metrics_job = {
    "user_name":        {
        "dbname": "user_name",
        "type": MType.STR},
    "used_queue":       {
        "dbname": "used_queue",
        "type": MType.STR},
    "submit_time":      {
        "dbname": "submit_time",
        "type": MType.INT},
    "start_time":       {
        "dbname": "start_time",
        "type": MType.INT},
    "end_time":         {
        "dbname": "end_time",
        "type": MType.INT},
    "run_time":         {
        "dbname": "run_time",
        "type": MType.INT},
    "requested_time":   {
        "dbname": "requested_time",
        "type": MType.INT},
    "requested_cu":     {
        "dbname": "requested_cu",
        "type": MType.INT},
    "num_nodes":        {
        "dbname": "num_nodes",
        "type": MType.INT},
    "exit_code":        {
        "dbname": "exit_code",
        "type": MType.INT},
    "alloc_cu": {
        "dbname": "alloc_cu",
        "type": MType.STR},
}

metrics_node = {
    "cpu_model": {
        "dbname": "cpu_model",
        "measurement": conf.measurements["node"],
        "query": QType.CUSTOM,
        "type": MType.STR},
    "sockets": {
        "dbname": "sockets",
        "measurement": conf.measurements["node"],
        "query": QType.CUSTOM,
        "type": MType.INT},
    "cores_per_socket": {
        "dbname": "cores_per_socket",
        "measurement": conf.measurements["node"],
        "query": QType.CUSTOM,
        "type": MType.INT},
    "phys_thr_core": {
        "dbname": "threads_per_core",
        "measurement": conf.measurements["node"],
        "query": QType.CUSTOM,
        "type": MType.INT},
    "virt_thr_core": {
        "dbname": "threads_per_core",
        "measurement": conf.measurements["node"],
        "query": QType.CUSTOM,
        "type": MType.INT},
    "main_mem": {
        "dbname": "main_mem",
        "measurement": conf.measurements["node"],
        "query": QType.CUSTOM,
        "type": MType.INT},
    "alloc_cu": {
        "dbname": "alloc_cu",
        "measurement": conf.measurements["node"],
        "query": QType.CUSTOM,
        "type": MType.INT},
    "alloc_mem": {
        "dbname": "alloc_mem",
        "measurement": conf.measurements["node"],
        "query": QType.CUSTOM,
        "type": MType.INT},
    "ib_xmit_max": {
        "dbname": "PortXmitData",
        "measurement": conf.measurements["infiniband"],
        "query": QType.MAX_OF_NODE,
        "type": MType.INT},
    "ib_rcv_max": {
        "dbname": "PortRcvData",
        "measurement": conf.measurements["infiniband"],
        "query": QType.MAX_OF_NODE,
        "type": MType.INT},
    "beegfs_write_sum": {
        "dbname": "MiB-wr",
        "type": MType.INT,
        "query": QType.SUM_OF_NODE,
        "measurement": conf.measurements["beegfs"],
    },
    "beegfs_read_sum": {
        "dbname": "MiB-rd",
        "type": MType.INT,
        "query": QType.SUM_OF_NODE,
        "measurement": conf.measurements["beegfs"],
    },
}

metrics_seq = {
    "proc_cpu_usage_sum": {
        "dbname": "cpu_usage",
        "type": MType.FLT,
        "query": QType.SUM_PER_NODE,
        "measurement": conf.measurements["proc"],
    },
    "proc_mem_rss_sum": {
        "dbname": "memory_rss",
        "type": MType.FLT,
        "query": QType.SUM_PER_NODE,
        "measurement": conf.measurements["proc"],
    },
    "proc_write_sum": {
        "dbname": "write_bytes",
        "type": MType.FLT,
        "query": QType.SUM_PER_NODE,
        "measurement": conf.measurements["proc"],
    },
    "proc_read_sum": {
        "dbname": "read_bytes",
        "type": MType.FLT,
        "query": QType.SUM_PER_NODE,
        "measurement": conf.measurements["proc"],
    },
    "node_cpu_usage_avg": {
        "dbname": "usage_user",
        "type": MType.FLT,
        "query": QType.AVG_OF_NODE,
        "measurement": conf.measurements["cpu"],
    },
    "node_cpu_idle_avg": {
        "dbname": "usage_idle",
        "type": MType.FLT,
        "query": QType.AVG_OF_NODE,
        "measurement": conf.measurements["cpu"],
    },
    "node_load_avg": {
        "dbname": "load1",
        "type": MType.FLT,
        "query": QType.AVG_OF_NODE,
        "measurement": conf.measurements["sys"],
    },
    "node_load_max": {
        "dbname": "load1",
        "type": MType.FLT,
        "query": QType.MAX_OF_NODE,
        "measurement": conf.measurements["sys"],
    },
    "node_ib_xmit_max": {
        "dbname": "PortXmitData",
        "type": MType.INT,
        "query": QType.MAX_OF_NODE,
        "measurement": conf.measurements["infiniband"],
    },
    "node_ib_rcv_max": {
        "dbname": "PortRcvData",
        "type": MType.INT,
        "query": QType.MAX_OF_NODE,
        "measurement": conf.measurements["infiniband"],
    },
    "node_beegfs_write_sum": {
        "dbname": "MiB-wr",
        "type": MType.INT,
        "query": QType.SUM_OF_NODE,
        "measurement": conf.measurements["beegfs"],
    },
    "node_beegfs_read_sum": {
        "dbname": "MiB-rd",
        "type": MType.INT,
        "query": QType.SUM_OF_NODE,
        "measurement": conf.measurements["beegfs"],
    },
}

metrics_gpu = {
    "gpu_power_limit": {
        "dbname": "power.limit",
        "type": MType.INT,
        "query": QType.LAST_OF_DEVICE,
        "measurement": conf.measurements["gpu_node"],
    },
    "gpu_memory_total": {
        "dbname": "memory.total",
        "type": MType.INT,
        "query": QType.LAST_OF_DEVICE,
        "measurement": conf.measurements["gpu_node"],
    },
    "gpu_temperature_max": {
        "dbname": "temperature.gpu",
        "type": MType.INT,
        "query": QType.MAX_PER_DEVICE,
        "measurement": conf.measurements["gpu_node"],
    },
    "gpu_power_max": {
        "dbname": "power.draw",
        "type": MType.FLT,
        "query": QType.MAX_PER_DEVICE,
        "measurement": conf.measurements["gpu_node"],
    },
    "gpu_memory_max": {
        "dbname": "memory.used",
        "type": MType.INT,
        "query": QType.MAX_PER_DEVICE,
        "measurement": conf.measurements["gpu_node"],
    },
    "gpu_utilization_max": {
        "dbname": "utilization.gpu",
        "type": MType.FLT,
        "query": QType.MAX_PER_DEVICE,
        "measurement": conf.measurements["gpu_node"],
    },
    "gpu_utilization_avg": {
        "dbname": "utilization.gpu",
        "type": MType.FLT,
        "query": QType.AVG_PER_DEVICE,
        "measurement": conf.measurements["gpu_node"],
    },
    "gpu_memory_avg_seq": {
        "dbname": "memory.used",
        "type": MType.FLT,
        "query": QType.AVG_PER_DEVICE_INT,
        "measurement": conf.measurements["gpu_node"],
    },
    "gpu_utilization_avg_seq": {
        "dbname": "utilization.gpu",
        "type": MType.FLT,
        "query": QType.AVG_PER_DEVICE_INT,
        "measurement": conf.measurements["gpu_node"],
    },
    "gpu_temperature_avg_seq": {
        "dbname": "temperature.gpu",
        "type": MType.INT,
        "query": QType.AVG_PER_DEVICE_INT,
        "measurement": conf.measurements["gpu_node"],
    },
    "gpu_power_avg_seq": {
        "dbname": "power.draw",
        "type": MType.FLT,
        "query": QType.AVG_PER_DEVICE_INT,
        "measurement": conf.measurements["gpu_node"],
    },
    "gpu_cpu_usage_max": {
        "dbname": "cpu_pcpu",
        "type": MType.INT,
        "query": QType.MAX_SUM_PER_INT,
        "measurement": conf.measurements["gpu_proc"],
    },
    "gpu_cpu_mem_max": {
        "dbname": "cpu_rss",
        "type": MType.INT,
        "query": QType.MAX_SUM_PER_INT,
        "measurement": conf.measurements["gpu_proc"],
    },
    "gpu_cpu_usage_sum_seq": {
        "dbname": "cpu_pcpu",
        "type": MType.INT,
        "query": QType.PROC_SUM_PER_DEVICE_INT,
        "measurement": conf.measurements["gpu_proc"],
    },
    "gpu_cpu_mem_sum_seq": {
        "dbname": "cpu_rss",
        "type": MType.INT,
        "query": QType.PROC_SUM_PER_DEVICE_INT,
        "measurement": conf.measurements["gpu_proc"],
    },
    "gpu_cpu_proc_count_total": {
        "dbname": "pid",
        "type": MType.INT,
        "query": QType.CUSTOM,
        "measurement": conf.measurements["gpu_proc"],
    },
    "gpu_cpu_proc_count_sum_seq": {
        "dbname": "pid",
        "type": MType.INT,
        "query": QType.CUSTOM,
        "measurement": conf.measurements["gpu_proc"],
    },

}
