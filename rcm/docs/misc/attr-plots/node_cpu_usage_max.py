import matplotlib.pyplot as plt
import numpy as np

XLIM = 64
YLIM = 120
x_const = 8

tx = np.arange(1.0, XLIM)

plt.axis([1, XLIM, 0, YLIM])
plt.ylabel('CPU usage (U, %)')
plt.xlabel('Requested cores (R)')
plt.grid(True, which="both", linestyle='--')

# lines
y_is_50 = tx*0 + 50
y_is_80 = tx*0 + 80
y_is_100 = tx*0 + 100
y_lim = tx*0 + YLIM
x_is_const = [x_const, x_const], [0, YLIM]

# plot the lines
plt.plot(tx, y_is_50, label="U=50%")
plt.plot(tx, y_is_80, label="U=80%")
plt.plot(tx, y_is_100, label="U=100%")
plt.plot(*x_is_const, label="R=8")

# highlight the area
plt.fill_between(tx, y_is_50, where=tx <= x_const, color='red', alpha=0.3)
plt.fill_between(tx, y_is_80, where=tx >= x_const, color='red', alpha=0.3)
plt.fill_between(tx, y_is_100, y_lim, color='orange', alpha=0.3)
plt.fill_between(tx, y_is_50, y_is_100, where=tx <= x_const, color='green', alpha=0.3)
plt.fill_between(tx, y_is_80, y_is_100, where=tx >= x_const, color='green', alpha=0.3)

plt.legend(loc='lower right')

plt.savefig('../../img/node_cpu_usage_max.svg')
plt.show()
