# The input of the recommendations system

The recommendation system is a python module and as an input it gets a python object.

The structure of the required python object is described here.

```python
class Aggregator:
    job = JobData()
    nodes = [NodeData()]
```

```python
class JobData:
    id = None
    user_name = None
    used_queue = None
    submit_time = None
    start_time = None
    end_time = None
    run_time = None
    requested_time = None
    requested_cu = None
    num_nodes = None
    alloc_cu = [AllocCUData()]

class AllocCUData:
    node_id = None
    cu_count = None
```

```python
class NodeData:
    name = None
    cpu_model = None
    sockets = None
    cores_per_socket = None
    virt_thr_core = None
    phys_thr_core = None
    main_mem = None
    seq_cpu_usage = None
    seq_load_avg = None
    seq_load_max = None
    seq_ib_rcv_max = None
    seq_ib_xmit_max = None
    proc = None
    alloc_cu = None
    alloc_mem = None
    ib_rcv_max = None
    ib_xmit_max = None

    def __init__(self):
        self.proc = ProcData()
        self.seq_load_avg = SeqVals()
        self.seq_load_max = SeqVals()
        self.seq_cpu_usage = SeqVals()
        self.seq_ib_rcv_max = SeqVals()
        self.seq_ib_xmit_max = SeqVals()

class SeqVals:
    delta = None
    seq = []

class ProcData:
    cpu_time_user = None
    cpu_time_system = None
    cpu_time_idle = None
    cpu_time_iowait = None
    write_bytes = None
    write_count = None
    read_bytes = None
    read_count = None
    mem_rss_max = None
    mem_rss_avg = None
    mem_swap_max = None
    mem_vms = None
    cpu_usage = None
    seq_cpu_usage = None
    seq_mem_rss_sum = None

    def __init__(self):
        self.seq_cpu_usage = SeqVals()
        self.seq_mem_rss_sum = SeqVals()
```
# Detailed structure of the object

## Aggregator

#### Aggregator.job
This attribute contains the object `JobData`. The information retrieved from the batch system.

#### Aggregator.nodes
This attribute contains the object of `NodeData`. The data collected from the nodes.

## JobData

#### JobData.id
**type**: string

The id of the job given by the batch system.

#### JobData.user_name
**type**: string

The username of the user who submitted the job.

#### JobData.used_queue
**type**: string

The queue / partiton name where the job was running

#### JobData.submit_time
**type**: integer. UNIX timestamp

Time when the job was submitted

#### JobData.start_time
**type**: integer. UNIX timestamp

Time when the job was started

#### JobData.end_time
**type**: integer. UNIX timestamp

Time when the job was ended

#### JobData.run_time
**type**: integer. seconds

The runtime of the job

#### JobData.requested_time
**type**: integer. seconds

Walltime requested by user for the job

#### JobData.num_nodes
**type**: integer

Amount of requested nodes

#### JobData.alloc_cu
**type**: list of [AllocCUData][#AllocCUData]

List of Compute Units (CU) allocation data

## AllocCUData

#### AllocCUData.node_id
**type**: string

The hostname of the node

#### AllocCUData.cu_count
**type**: integer

The allocated Compute Units (CU) on the corresponding node.

## NodeData

#### NodeData.name
**type**: string

The hostname of the node

#### NodeData.cpu_model
**type**: string

The CPU model of the node

#### NodeData.sockets
**type**: integer

Amount of sockets on the node

#### NodeData.cores_per_socket
**type**: integer

Amount of cores per socket

#### NodeData.virt_thr_core
**type**: integer

Amount of virtual threads per core

#### NodeData.phys_thr_core
**type**: integer

Amount of physical threads per core

#### NodeData.main_mem
**type**: integer. Bytes

Amount of RAM on the node

#### NodeData.alloc_cu
**type**: integer

Amount of allocated CU on the node

#### NodeData.ib_rcv_max
**type**: integer. Bytes per second

The maximum (high water mark) rate of received data over infiniband in bytes per second

#### NodeData.ib_xmit_max
**type**: integer. Bytes per second

The maximum (high water mark) rate of transmitted data over infiniband in bytes per second

#### NodeData.proc
**type**: [ProcData](#ProcData)

Aggregated data of job's processes on the node.

#### NodeData.seq_cpu_usage
**type**: [SeqVals](#SeqVals)

Sequential data of cpu usage. aggregated as an average in interval.

#### NodeData.seq_load_avg
**type**: [SeqVals](#SeqVals)

Sequential data of load1 of the node. aggregated as an average in interval.

#### NodeData.seq_load_max
**type**: [SeqVals](#SeqVals)

Sequential data of load1 of the node. aggregated as maximum in interval.

#### NodeData.seq_ib_rcv_max
**type**: [SeqVals](#SeqVals)

Sequential data of received data rate over infiniband. aggregated as maximum bytes per second in interval.

#### NodeData.seq_ib_xmit_max
**type**: [SeqVals](#SeqVals)

Sequential data of transmitted data rate over infiniband. aggregated as maximum bytes per second in interval.

## SeqVals

#### SeqVals.delta
**type**: integer. seconds

The difference between every consecutive points in the sequence.

#### SeqVals.seq
**type**: list of arbitrary data types

The list of data points. The first point corresponds to the data point at the time [JobData.start_time](#JobData.start_time) and the last point corresponds to the data point at the time [JobData.end_time](#JobData.end_time)

## ProcData

#### ProcData.cpu_time_user
**type**: integer. seconds

The sum of the time CPU spent on running user processes/calls.

#### ProcData.cpu_time_system
**type**: integer. seconds

The sum of the time CPU spent on running system processes/calls.

#### ProcData.cpu_time_idle
**type**: integer. seconds

The sum of the time CPU was idling.

#### ProcData.cpu_time_iowait
**type**: integer. seconds

The sum of the time CPU spent waiting for IO.

#### ProcData.write_bytes
**type**: integer. bytes

The maximum (high water mark) amount of bytes processes wrote to FS.

#### ProcData.read_bytes
**type**: integer. bytes

The maximum (high water mark) amount of bytes processes read from FS.

#### ProcData.write_count
**type**: integer

The maximum (high water mark) amount of write accesses to FS.

#### ProcData.read_count
**type**: integer

The maximum (high water mark) amount of read accesses to FS.

#### ProcData.mem_rss_max
**type**: integer. bytes

The maximum (high water mark) amount of RSS memory occupied by the job on the node.

#### ProcData.mem_rss_avg
**type**: integer. bytes

The average amount of RSS memory occupied by the job on the node.

#### ProcData.mem_swap_max
**type**: integer. bytes

The maximum (high water mark) amount of swaped memory on the node.

#### ProcData.mem_vms
**type**: integer. bytes

The average amount of VMS memory on the node.

#### ProcData.cpu_usage
**type**: integer. bytes

The maximum (high water mark) of cpu usage sum of all processes running in the job on the node.

#### ProcData.seq_cpu_usage
**type**: [SeqVals](#SeqVals)

Sequential data of cpu usage sum of all processes running in the job on the node aggregated as an average on the interval.

#### ProcData.seq_mem_rss_sum
**type**: [SeqVals](#SeqVals)

Sequential data of RSS memory usage sum of all processes running in the job on the node aggregated as an average on the interval.
