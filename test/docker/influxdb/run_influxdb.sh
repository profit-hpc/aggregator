#! /bin/bash

# port for local binding
IPORT=58086
ITAG="pfit-influxdb"
ROOTOFREP=`git rev-parse --show-toplevel`
IDB="pfit"
CONNAME=$ITAG-container
JOBIDS="2368599 1352076"

if [[ "$(docker images -q $ITAG 2> /dev/null)" == "" ]]; then
  $ROOTOFREP/test/docker/influxdb/build_influxdb.sh "$ITAG"
fi

# run influxdb in container
if [ ! "$(docker ps -q -f name=$CONNAME)" ]; then
  docker run -d --rm --name $CONNAME -p $IPORT:8086 -it $ITAG /bin/bash
fi

sleep 2

# remove old db
curl -i -XPOST http://localhost:$IPORT/query --data-urlencode "q=DROP DATABASE $IDB"

# create db
curl -i -XPOST http://localhost:$IPORT/query --data-urlencode "q=CREATE DATABASE $IDB"

# ingest data points
for JOBID in $JOBIDS; do
  for i in $(ls $ROOTOFREP/test/data/${JOBID}/influx/*); do
  	curl -i -XPOST "http://localhost:$IPORT/write?db=$IDB" --data-binary @$i
  done
done
