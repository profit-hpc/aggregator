#! /usr/bin/env python3

import json
import argparse
import db.aggregator as aggregator
import conf.config as conf
import subprocess
import re
import os
import gzip
import io
from format import format
from rcm import rcm

def cache_file(jobid, filename, content):
    if conf.CACHEJSON is False or conf.CACHEDIR == "":
        return False

    if not os.path.exists(conf.CACHEDIR):
        os.makedirs(conf.CACHEDIR, mode=0o770)

    cachefile = conf.CACHEDIR + "/" + filename

    fd = os.open(cachefile, os.O_CREAT | os.O_WRONLY, 0o660)
    f = gzip.GzipFile(fileobj=io.FileIO(fd, mode='wb'))
    f.write(content.encode())
    f.close()

    return True

def cache_data(jobid, type, content):
    if conf.CACHEJSON is False or conf.CACHEDIR == "":
        return False

    filename = ""
    if type == "text":
        filename = "{:s}.txt.json.gz".format(jobid)
    elif type == "pdf":
        filename = "{:s}.pdf.json.gz".format(jobid)
    else:
        raise RuntimeError("invalid type of report: {}".format(type))

    cache_file(jobid, filename, content)


def merge_and_out(job_id, aggr, rcm, type, cache, out_dir=None):
    if cache[type] is False:
        formatted = format.format(aggr, type)
        formatted["recommendations"] = rcm
        jsonstr = json.dumps(formatted)

        if conf.CACHEJSON is True:
            cache_data(job_id, type, jsonstr)
    else:
        jsonstr = cache[type]

    if out_dir is None:
        print(jsonstr)
    else:
        filename = "{:s}/{:s}.{:s}.json".format(out_dir, job_id, type)
        os.seteuid(os.getuid())
        with open(filename, 'w') as outfile:
            outfile.write(jsonstr)

        print("{:s} data was exported in {:s}".format(type, filename))

def check_user(jobid):
    euid = os.geteuid()
    uid = os.getuid()

    if euid == 0 or uid == 0:
        return

    if conf.BATCH_SYSTEM == "SLURM":
        job_uid_comm = conf.job_uid_comm["slurm"].format(jobid = jobid)
    elif conf.BATCH_SYSTEM == "LSF":
        job_uid_comm = conf.job_uid_comm["lsf"].format(jobid = jobid)
    else:
        print("Cannot check the user ID, no batch system specified")
        exit(1)

    result = subprocess.run(job_uid_comm, stdout=subprocess.PIPE, shell=True,
                            executable='/bin/bash')
    out = result.stdout.decode("utf-8")
    m = re.search("[0-9]+", out)

    if m is None or m.group(0) is None:
        print("Cannot parse UID. {:s}".format(out))
        exit(1)

    job_uid = m.group(0)

    if int(job_uid) != int(uid):
        print("Access denied. UIDs of the user and the job do not match.")
        exit(1)

    return

def job_id_transform(jobid):
    if "_" not in jobid:
        return jobid

    if conf.BATCH_SYSTEM == "SLURM":
        jobid_comm = conf.jobid_array_trans_comm["slurm"].format(jobid = jobid)
    elif conf.BATCH_SYSTEM == "LSF":
        return jobid
    else:
        print("Cannot transform JOBID, no batch system specified")
        exit(1)

    result = subprocess.run(jobid_comm, stdout=subprocess.PIPE, shell=True,
                            executable='/bin/bash')
    out = result.stdout.decode("utf-8")
    m = re.search("[0-9]+", out)

    if m is None or m.group(0) is None:
        print("Cannot parse JOBID. {:s}".format(out))
        exit(1)

    raw_job_id = m.group(0)

    return raw_job_id

def check_cache_file(jobid, filename):
    if conf.CACHEJSON is False or conf.CACHEDIR == "":
        return False

    if not os.path.exists(conf.CACHEDIR):
        os.makedirs(conf.CACHEDIR, mode=0o770)

    cachefile = conf.CACHEDIR + "/" + filename

    res = False
    if os.path.exists(cachefile):
        f = gzip.open(cachefile, "rt")
        content = f.read()
        if content != "init" and content != "":
            res = content
    else:
        fd = os.open(cachefile, os.O_CREAT | os.O_WRONLY, 0o660)
        f = gzip.GzipFile(fileobj=io.FileIO(fd, mode='wb'))
        f.write("init".encode())
        f.close()

    return res

def check_cache_txt(jobid):
    txtfilename = "{:s}.txt.json.gz".format(jobid)
    return check_cache_file(jobid, txtfilename)

def check_cache_pdf(jobid):
    txtfilename = "{:s}.pdf.json.gz".format(jobid)
    return check_cache_file(jobid, txtfilename)

def main():
    parser = argparse.ArgumentParser(description="""
        Gets the job information required for generating text or PDF reports
        and outputs it in JSON format.
    """, formatter_class=argparse.ArgumentDefaultsHelpFormatter)

    parser.add_argument("-t", "--type", help="type of the output",
                        choices=['text', 'pdf', 'all'], default="text")

    parser.add_argument("-o", "--output-dir", help="output directory",
                        const="./out", required=False, nargs='?')

    parser.add_argument("JOBID",
                        help="job ID used in the batch system")

    args = parser.parse_args()

    job_id = args.JOBID

    if conf.SECUSER:
        check_user(job_id)

    # Errors in arguments
    if args.output_dir is None:
        if args.type == "all":
            print("Cannot print both data in STDOUT")
            exit()
    # End of errors in arguments

    if conf.JOBID_ARRAY_TRANSFORM:
        job_id = job_id_transform(job_id)

    # initial values
    need_to_aggregate = True
    cache = {"text": False, "pdf": False}
    aggr = None
    recommendations = None

    # Check if we can use cached data
    if conf.CACHEJSON is True:
        if args.type == "all":
            cache["text"] = check_cache_txt(job_id)
            cache["pdf"] = check_cache_pdf(job_id)
            need_to_aggregate = (cache["text"] is False) or (cache["pdf"] is False)
        elif args.type == "text":
            cache["text"] = check_cache_txt(job_id)
            need_to_aggregate = cache["text"] is False
        elif args.type == "pdf":
            cache["pdf"] = check_cache_pdf(job_id)
            need_to_aggregate = cache["pdf"] is False

    if need_to_aggregate is True:
        aggr = aggregator.get_aggregator(job_id, "pdf")
        try:
            recommendations = rcm.get_recommendations(aggr)
        except:
            recommendations = ["Error occured during recommendations generation"]

    if args.output_dir is None:
        merge_and_out(job_id, aggr, recommendations, args.type, cache)
    else:
        if args.type == "all":
            merge_and_out(job_id, aggr, recommendations, "text", cache, args.output_dir)
            merge_and_out(job_id, aggr, recommendations, "pdf", cache, args.output_dir)
        else:
            merge_and_out(job_id, aggr, recommendations, args.type, cache, args.output_dir)

    return 0

if __name__ == "__main__":
    main()
