# Recommendation system

This is the implementation of the recommendation system in the project ProfiT-HPC.

The main components of the system are:

- attributes
- rules

The attributes are functions which check some basic parameters and return a single value from the specified set of values.

Rules are formed by combining the attributes and assuming that they equal to a particular value.

The corresponding documentation on the attributes and rules implemented in the system can be found at `./docs`, namely at [docs/attributes](./docs/attributes.md) and  [docs/rules](./docs/rules.md)
