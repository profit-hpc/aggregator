#!/usr/bin/env python3
import os

# Begin import RULES from the array
cur_path = os.path.realpath(__file__)
rules_path = os.path.dirname(cur_path) + "/../../../rules.py"

exec(open(rules_path).read())
# End import

def print_header():
    print("# Rules \n"
          "A rule is a set of [attributes](./attributes.md) with specified values."
          "\n")

def format_attr_link(attr):
    return "./attributes.md#{}".format(attr)

def print_rule(rule):
    print("---")
    print("#### recommendation")
    print("```")
    print(rule["msg"])
    print("```")
    print("#### attributes")
    print("attribute name | value(s)")
    print("--- | ---")
    for attr_name, attr_value in rule["attrs"].items():
        attrs = ""
        if isinstance(attr_value, list):
            attrs = ", ".join(attr_value)
        else:
            attrs = attr_value

        print("[{an}]({al}) | `{av}`".format(
            an = attr_name,
            al = format_attr_link(attr_name),
            av = attrs,
            ))
    print("")

def main():
    print_header()
    for arule in RULES:
        print_rule(arule)

if __name__ == "__main__":
    main()
