#!/bin/bash
#
# ProfiT-HPC postexecution script
# This script will be run by the batch system after the execution of the job.
# The script calls Python script to retrieve data from Batch system and then
# write it to DB
#
# Created by Azat Khuziyakhmetov, azat.khuziyakhmetov@gwdg.de
# Date: 2018-09-25

# optional debugging, silence otherwise
DEBUG=true

PFIT_BATCH_SYSTEM="LSF"
BASEDIR=$(dirname "$0")

case $PFIT_BATCH_SYSTEM in
  LSF) #do LSF specific stuff
    PFIT_JOBID=${LSB_JOBID}
    module load python/3.5.1
    RES=`python3 $BASEDIR/../data.py -s $PFIT_JOBID`
    ;;
  SLURM) #do Slurm specific stuff
    echo "2b implemented"
    RES=`python3 $BASEDIR/../data.py -s $PFIT_JOBID`
    ;;
  TORQUE) #do Torque specific stuff
    echo "2b implemented"
    RES=`python3 $BASEDIR/../data.py -s $PFIT_JOBID`
    ;;
esac

exit $RES

