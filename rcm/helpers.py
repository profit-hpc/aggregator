import sys
import math
from db.aggrstruct import *

LONG_DUR_SEC = 300
GPU_LONG_DUR_SEC = 1800

def accepts(*types, **kw):
    '''Function decorator. Checks decorated function's arguments are
    of the expected types.

    Parameters:
    types -- The expected types of the inputs to the decorated function.
             Must specify type for each parameter.
    kw    -- Optional specification of 'debug' level (this is the only valid
             keyword argument, no other should be given).
             debug = ( 0 | 1 | 2 )

    '''
    if not kw:
        # default level: MEDIUM
        debug = 1
    else:
        debug = kw['debug']
    try:
        def decorator(f):
            def newf(*args):
                if debug == 0:
                    return f(*args)
                assert len(args) == len(types)
                argtypes = tuple(map(type, args))
                if argtypes != types:
                    msg = info(f.__name__, types, argtypes, 0)
                    if debug == 1:
                        print >> sys.stderr, 'TypeWarning: ', msg
                    elif debug == 2:
                        raise TypeError(msg)
                return f(*args)
            newf.__name__ = f.__name__
            return newf
        return decorator
    except KeyError:
        key = KeyError.args
        raise KeyError(key + "is not a valid keyword argument")
    except TypeError:
        msg = TypeError.args
        raise TypeError(msg)

@accepts(NodeData)
def node_has_high_load_interval(node):
    has_high_load = False

    conseq_points = 0

    seq = node.seq_load_max

    max_points = math.ceil(LONG_DUR_SEC / seq.delta)
    max_load = node.sockets * node.cores_per_socket * (node.phys_thr_core + node.virt_thr_core)

    for p in seq.seq:
        if conseq_points >= max_points:
            has_high_load = True
            break

        if p is None: continue

        if p >= max_load:
            conseq_points += 1
        else:
            conseq_points = 0

    return has_high_load

@accepts(GPUData)
def gpu_has_overcrowded_interval(gpu):
    has_overcrowded = False

    conseq_points = 0

    seq = gpu.seq_cpu_proc_count

    max_points = math.ceil(GPU_LONG_DUR_SEC / seq.delta)

    for p in seq.seq:
        if conseq_points >= max_points:
            has_overcrowded = True
            break

        if p is None: continue

        if p >= 2:
            conseq_points += 1
        else:
            conseq_points = 0

    return has_overcrowded
