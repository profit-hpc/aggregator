import matplotlib.pyplot as plt
import numpy as np

XLIM = 32
YLIM = 20

tx = np.arange(0.0, XLIM)

plt.axis([0, XLIM, 0, YLIM])
plt.ylabel('Used walltime (U, hours)')
plt.xlabel('Requested walltime (R, hours)')
plt.grid(True, which="both", linestyle='--')

# lines
x_const = 4
x_is_const = [x_const, x_const], [0, YLIM]
half_x = tx/2
y_is_less = tx - 6

# plot the lines
plt.plot(tx, tx, label="R=U")
plt.plot(*x_is_const, label="R=4")
plt.plot(tx, half_x, label="U=R/2")
plt.plot(tx, y_is_less, label="U=R-6")

# highlight the area
plt.fill_between(tx, half_x, where=tx >= x_const, color='red', alpha=0.3)
plt.fill_between(tx, y_is_less, half_x, where=y_is_less >= half_x, color='red', alpha=0.3)

plt.legend(loc='lower right')

plt.savefig('../../img/req_walltime.svg')
plt.show()
