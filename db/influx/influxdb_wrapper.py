import conf.config as conf
from .influxdb_fetchjob import get_job_data
from .influxdb_fetchproc import get_proc_data
from .influxdb_fetchnode import get_node_data
from .influxdb_fetchseq import get_seq_data
from .influxdb_fetchgpu import get_gpu_data
from ..aggrstruct import *
from . import common

def fetch_all(job_id, type):
    data = {}
    data["job"], data["alloc_cu"], data["alloc_mem"] = get_job_data(job_id)
    t_start = data["job"]["start_time"]
    t_end = t_start + data["job"]["run_time"]

    if data["job"]["run_time"] < conf.MIN_DUR * 60:
        common.eprint("The runtime of the job ({dur:d} seconds) is too short. Minimum is {min:d} seconds".format(
            dur = int(data["job"]["run_time"]),
            min = int(conf.MIN_DUR * 60),
        ))

    data["proc"] = get_proc_data(job_id)

    node_ids = list(data["proc"].keys())

    data["node"] = get_node_data(node_ids, t_start, t_end)

    if type == "pdf":
        data["seq"], data["seq_delta"] = get_seq_data(job_id, t_start, t_end, node_ids)

    if conf.GPU is True:
        data["gpu"] = get_gpu_data(job_id, t_start, t_end, node_ids)

    return data

def get_aggregator(job_id, type="text"):
    data = fetch_all(job_id, type)

    aggr = Aggregator()

    jd = data["job"]
    cud = data["alloc_cu"]
    memd = data["alloc_mem"]

    aggr.job.id = job_id
    aggr.job.user_name = jd.get("user_name")
    aggr.job.used_queue = jd.get("used_queue")
    aggr.job.submit_time = jd.get("submit_time")
    aggr.job.start_time = jd.get("start_time")
    aggr.job.end_time = jd.get("start_time") + jd.get("run_time")
    aggr.job.run_time = jd.get("run_time")
    aggr.job.requested_time = jd.get("requested_time")
    aggr.job.requested_cu = jd.get("requested_cu")
    aggr.job.num_nodes = jd.get("num_nodes")
    aggr.job.exit_code = jd.get("exit_code")

    allocated_nodes = []
    aggr.job.alloc_cu = []
    for node_id, cu_count in cud.items():
        allocated_nodes.append(node_id)
        aggr.job.alloc_cu.append(AllocCUData(node_id, cu_count))

    aggr.nodes = []
    for node_id in sorted(data["node"]):
        if node_id not in allocated_nodes:
            continue

        noded = data["node"][node_id]
        new_node = NodeData()

        new_node.name = node_id
        new_node.cpu_model = noded.get("cpu_model")
        new_node.sockets = noded.get("sockets")
        new_node.cores_per_socket = noded.get("cores_per_socket")
        new_node.virt_thr_core = noded.get("virt_thr_core")
        new_node.phys_thr_core = noded.get("phys_thr_core")
        new_node.main_mem = noded.get("main_mem")
        new_node.alloc_cu = cud.get(node_id)
        amem = memd.get(node_id)
        if amem > 0:
            new_node.alloc_mem = memd.get(node_id)
        else:
            ncpus = noded.get("sockets") * noded.get("cores_per_socket") * noded.get("virt_thr_core")
            nmem = int(cud.get(node_id) * noded.get("main_mem") / ncpus / 1000000) #in MB
            new_node.alloc_mem = nmem

        # for now we just account hyperthreading
        new_node.phys_thr_core = 1
        new_node.virt_thr_core -= 1

        if conf.INFINIBAND is True:
            new_node.ib_rcv_max = noded.get("ib_rcv_max")
            new_node.ib_xmit_max = noded.get("ib_xmit_max")
            # Multiply infiniband metrics by 4 if exist. To get bytes
            if new_node.ib_rcv_max is not None:
                new_node.ib_rcv_max *= 4
            if new_node.ib_xmit_max is not None:
                new_node.ib_xmit_max *= 4

        if conf.BEEGFS is True:
            new_node.beegfs_read_sum = noded.get("beegfs_read_sum")
            new_node.beegfs_write_sum = noded.get("beegfs_write_sum")
            # Multiply beegfs metrics by 1024 if exist. To get bytes
            if new_node.beegfs_read_sum is not None:
                new_node.beegfs_read_sum *= 1024
            if new_node.beegfs_write_sum is not None:
                new_node.beegfs_write_sum *= 1024

        procd = data["proc"].get(node_id)

        new_node.proc.cpu_time_user = procd.get("cpu_time_user")
        new_node.proc.cpu_time_system = procd.get("cpu_time_system")
        new_node.proc.cpu_time_idle = procd.get("cpu_time_idle")
        new_node.proc.cpu_time_iowait = procd.get("cpu_time_iowait")
        new_node.proc.write_bytes = procd.get("write_bytes")
        new_node.proc.write_count = procd.get("write_count")
        new_node.proc.read_bytes = procd.get("read_bytes")
        new_node.proc.read_count = procd.get("read_count")
        new_node.proc.mem_rss_max = procd.get("mem_rss_max")
        new_node.proc.mem_rss_avg = procd.get("mem_rss_avg")
        new_node.proc.mem_swap_max = procd.get("mem_swap_max")
        new_node.proc.mem_vms = procd.get("mem_vms")
        new_node.proc.cpu_usage = procd.get("cpu_usage")

        new_node.gpus = []
        if conf.GPU is True:
            gpud = data["gpu"].get(node_id, None)

            if gpud is not None:
                busses = []
                for bus_id, busd in gpud.items():
                    newbus = GPUData()

                    newbus.bus = bus_id
                    newbus.power_limit = busd["gpu_power_limit"]
                    newbus.mem = busd["gpu_memory_total"]
                    newbus.mem_max = busd["gpu_memory_max"]
                    newbus.temp_max = busd["gpu_temperature_max"]
                    newbus.power_max = busd["gpu_power_max"]
                    newbus.usage_max = busd["gpu_utilization_max"]
                    newbus.usage_avg = busd["gpu_utilization_avg"]
                    newbus.cpu_usage_max = busd["gpu_cpu_usage_max"]
                    newbus.cpu_mem_rss_max = busd["gpu_cpu_mem_max"]
                    newbus.cpu_proc_total = busd["gpu_cpu_proc_count_total"]

                    busses.append(newbus)

                new_node.gpus = busses

        if type == "pdf":
            new_node.proc.seq_cpu_usage = SeqVals(data["seq_delta"], data["seq"][node_id].get("proc_cpu_usage_sum"))
            new_node.proc.seq_mem_rss_sum = SeqVals(data["seq_delta"], data["seq"][node_id].get("proc_mem_rss_sum"))
            new_node.proc.seq_write_sum = SeqVals(data["seq_delta"], data["seq"][node_id].get("proc_write_sum"))
            new_node.proc.seq_read_sum = SeqVals(data["seq_delta"], data["seq"][node_id].get("proc_read_sum"))
            new_node.seq_cpu_usage = SeqVals(data["seq_delta"], data["seq"][node_id].get("node_cpu_idle_avg"))
            new_node.seq_cpu_usage.seq = [None if x is None else (100 - x) for x in new_node.seq_cpu_usage.seq]

            new_node.seq_load_avg = SeqVals(data["seq_delta"], data["seq"][node_id].get("node_load_avg"))
            new_node.seq_load_max = SeqVals(data["seq_delta"], data["seq"][node_id].get("node_load_max"))

            if conf.INFINIBAND is True:
                new_node.seq_ib_rcv_max = SeqVals(data["seq_delta"], data["seq"][node_id].get("node_ib_rcv_max"))
                new_node.seq_ib_xmit_max = SeqVals(data["seq_delta"], data["seq"][node_id].get("node_ib_xmit_max"))
                # Multiply values by 4 in order to get bytes
                if new_node.seq_ib_rcv_max.seq is not None:
                    new_node.seq_ib_rcv_max.seq = [None if x is None else (4 * x) for x in new_node.seq_ib_rcv_max.seq]
                if new_node.seq_ib_xmit_max.seq is not None:
                    new_node.seq_ib_xmit_max.seq = [None if x is None else (4 * x) for x in new_node.seq_ib_xmit_max.seq]

            if conf.BEEGFS is True:
                new_node.seq_beegfs_read_sum = SeqVals(data["seq_delta"], data["seq"][node_id].get("node_beegfs_read_sum"))
                new_node.seq_beegfs_write_sum = SeqVals(data["seq_delta"], data["seq"][node_id].get("node_beegfs_write_sum"))
                # Multiply values by 4 in order to get bytes
                if new_node.seq_beegfs_read_sum.seq is not None:
                    new_node.seq_beegfs_read_sum.seq = [None if x is None else (4 * x) for x in new_node.seq_beegfs_read_sum.seq]
                if new_node.seq_beegfs_write_sum.seq is not None:
                    new_node.seq_beegfs_write_sum.seq = [None if x is None else (4 * x) for x in new_node.seq_beegfs_write_sum.seq]

            if conf.GPU is True and gpud is not None:
                for i in range(0, len(new_node.gpus)):
                    bus_id = new_node.gpus[i].bus
                    busd = data["gpu"][node_id][bus_id]

                    new_node.gpus[i].seq_mem_avg = SeqVals(data["seq_delta"], busd["gpu_memory_avg_seq"])
                    new_node.gpus[i].seq_usage_avg = SeqVals(data["seq_delta"], busd["gpu_utilization_avg_seq"])
                    new_node.gpus[i].seq_temp_avg = SeqVals(data["seq_delta"], busd["gpu_temperature_avg_seq"])
                    new_node.gpus[i].seq_power_avg = SeqVals(data["seq_delta"], busd["gpu_power_avg_seq"])
                    new_node.gpus[i].seq_cpu_usage_sum = SeqVals(data["seq_delta"], busd["gpu_cpu_usage_sum_seq"])
                    new_node.gpus[i].seq_cpu_mem_rss_sum = SeqVals(data["seq_delta"], busd["gpu_cpu_mem_sum_seq"])
                    new_node.gpus[i].seq_cpu_proc_count = SeqVals(data["seq_delta"], busd["gpu_cpu_proc_count_sum_seq"])

        aggr.nodes.append(new_node)

    return aggr
