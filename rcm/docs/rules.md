# Rules 
A rule is a set of [attributes](./attributes.md) with specified values.

---
#### recommendation
```
Some nodes are overloaded. Probably by other processes
```
#### attributes
attribute name | value(s)
--- | ---
[gpu_job](./attributes.md#gpu_job) | `False`
[job_nodes_amount](./attributes.md#job_nodes_amount) | `MULT`
[cpu_usage_total_max](./attributes.md#cpu_usage_total_max) | `NORM`
[overloaded_node_exists](./attributes.md#overloaded_node_exists) | `True`

---
#### recommendation
```
The requested walltime is too hight. Try to request less time
```
#### attributes
attribute name | value(s)
--- | ---
[req_walltime](./attributes.md#req_walltime) | `HIGH`

---
#### recommendation
```
The CPU usage on some nodes is low, please adjust the amount of requested resources or workload of the job
```
#### attributes
attribute name | value(s)
--- | ---
[gpu_job](./attributes.md#gpu_job) | `False`
[job_nodes_amount](./attributes.md#job_nodes_amount) | `MULT`
[req_walltime](./attributes.md#req_walltime) | `NORM`
[cpu_usage_total_max](./attributes.md#cpu_usage_total_max) | `LOW`
[node_cpu_usage_min](./attributes.md#node_cpu_usage_min) | `LOW`
[node_cpu_usage_max](./attributes.md#node_cpu_usage_max) | `NORM`

---
#### recommendation
```
Some nodes were not used during the runtime
```
#### attributes
attribute name | value(s)
--- | ---
[job_nodes_amount](./attributes.md#job_nodes_amount) | `MULT`
[req_walltime](./attributes.md#req_walltime) | `NORM`
[node_cpu_usage_min](./attributes.md#node_cpu_usage_min) | `ZERO`
[node_cpu_usage_max](./attributes.md#node_cpu_usage_max) | `NORM`
[gpu_usage_min](./attributes.md#gpu_usage_min) | `ZERO`

---
#### recommendation
```
The CPU usage is not distributed equally among the nodes. Try to use the nodes evenly
```
#### attributes
attribute name | value(s)
--- | ---
[gpu_job](./attributes.md#gpu_job) | `False`
[job_nodes_amount](./attributes.md#job_nodes_amount) | `MULT`
[req_walltime](./attributes.md#req_walltime) | `NORM`
[cpu_usage_total_max](./attributes.md#cpu_usage_total_max) | `NORM`
[node_cpu_usage_min](./attributes.md#node_cpu_usage_min) | `LOW`
[node_cpu_usage_max](./attributes.md#node_cpu_usage_max) | `NORM`

---
#### recommendation
```
The CPU usage of the job is low on all nodes, please request appropriate amount of resources
```
#### attributes
attribute name | value(s)
--- | ---
[gpu_job](./attributes.md#gpu_job) | `False`
[job_nodes_amount](./attributes.md#job_nodes_amount) | `MULT`
[req_walltime](./attributes.md#req_walltime) | `NORM`
[mem_usage_total](./attributes.md#mem_usage_total) | `LOW`
[cpu_usage_total_max](./attributes.md#cpu_usage_total_max) | `LOW`
[node_cpu_usage_min](./attributes.md#node_cpu_usage_min) | `LOW`
[node_cpu_usage_max](./attributes.md#node_cpu_usage_max) | `LOW`

---
#### recommendation
```
The CPU usage of the job is low on all nodes. Please check if the job is running properly
```
#### attributes
attribute name | value(s)
--- | ---
[gpu_job](./attributes.md#gpu_job) | `False`
[job_nodes_amount](./attributes.md#job_nodes_amount) | `MULT`
[req_walltime](./attributes.md#req_walltime) | `NORM`
[mem_usage_total](./attributes.md#mem_usage_total) | `NORM`
[cpu_usage_total_max](./attributes.md#cpu_usage_total_max) | `LOW`
[node_cpu_usage_min](./attributes.md#node_cpu_usage_min) | `LOW`
[node_cpu_usage_max](./attributes.md#node_cpu_usage_max) | `LOW`

---
#### recommendation
```
The CPU usage of the node is low. It might indicate that the job is not running in full power
```
#### attributes
attribute name | value(s)
--- | ---
[gpu_job](./attributes.md#gpu_job) | `False`
[job_nodes_amount](./attributes.md#job_nodes_amount) | `ONE`
[req_walltime](./attributes.md#req_walltime) | `NORM`
[node_cpu_usage_max](./attributes.md#node_cpu_usage_max) | `LOW`

---
#### recommendation
```
The node is overloaded. Probably by other processes on the node
```
#### attributes
attribute name | value(s)
--- | ---
[job_nodes_amount](./attributes.md#job_nodes_amount) | `ONE`
[req_walltime](./attributes.md#req_walltime) | `NORM`
[node_cpu_usage_max](./attributes.md#node_cpu_usage_max) | `NORM`
[overloaded_node_exists](./attributes.md#overloaded_node_exists) | `True`

---
#### recommendation
```
The node is overloaded and cpu usage of job was high
```
#### attributes
attribute name | value(s)
--- | ---
[job_nodes_amount](./attributes.md#job_nodes_amount) | `ONE`
[req_walltime](./attributes.md#req_walltime) | `NORM`
[node_cpu_usage_max](./attributes.md#node_cpu_usage_max) | `HIGH`
[overloaded_node_exists](./attributes.md#overloaded_node_exists) | `True`

---
#### recommendation
```
Swap was used on one of the nodes
```
#### attributes
attribute name | value(s)
--- | ---
[job_nodes_amount](./attributes.md#job_nodes_amount) | `MULT`
[mem_swap_used](./attributes.md#mem_swap_used) | `True`

---
#### recommendation
```
Swap was used on the node
```
#### attributes
attribute name | value(s)
--- | ---
[job_nodes_amount](./attributes.md#job_nodes_amount) | `ONE`
[mem_swap_used](./attributes.md#mem_swap_used) | `True`

---
#### recommendation
```
The single GPU has low usage
```
#### attributes
attribute name | value(s)
--- | ---
[gpu_job](./attributes.md#gpu_job) | `True`
[gpus_amount](./attributes.md#gpus_amount) | `ONE`
[gpu_usage_max](./attributes.md#gpu_usage_max) | `LOW`

---
#### recommendation
```
All GPUs have low usage
```
#### attributes
attribute name | value(s)
--- | ---
[gpu_job](./attributes.md#gpu_job) | `True`
[gpus_amount](./attributes.md#gpus_amount) | `MULT`
[gpu_usage_max](./attributes.md#gpu_usage_max) | `LOW`

---
#### recommendation
```
Some of the GPUs have low usage which might be caused by workload inbalance
```
#### attributes
attribute name | value(s)
--- | ---
[gpu_job](./attributes.md#gpu_job) | `True`
[gpus_amount](./attributes.md#gpus_amount) | `MULT`
[gpu_usage_max](./attributes.md#gpu_usage_max) | `NORM, HIGH`
[gpu_usage_min](./attributes.md#gpu_usage_min) | `LOW`

---
#### recommendation
```
Some of the GPUs were not used
```
#### attributes
attribute name | value(s)
--- | ---
[gpu_job](./attributes.md#gpu_job) | `True`
[gpus_amount](./attributes.md#gpus_amount) | `MULT`
[gpu_usage_min](./attributes.md#gpu_usage_min) | `ZERO`

---
#### recommendation
```
None of the GPUs were used
```
#### attributes
attribute name | value(s)
--- | ---
[gpu_job](./attributes.md#gpu_job) | `True`
[gpus_amount](./attributes.md#gpus_amount) | `MULT`
[gpu_usage_max](./attributes.md#gpu_usage_max) | `ZERO`

---
#### recommendation
```
Some GPUs have more than 1 process using them at the same time
```
#### attributes
attribute name | value(s)
--- | ---
[gpu_job](./attributes.md#gpu_job) | `True`
[gpus_amount](./attributes.md#gpus_amount) | `MULT`
[gpus_overcrowded_exist](./attributes.md#gpus_overcrowded_exist) | `True`

---
#### recommendation
```
The GPU has been used by more than 1 process simultaneously
```
#### attributes
attribute name | value(s)
--- | ---
[gpu_job](./attributes.md#gpu_job) | `True`
[gpus_amount](./attributes.md#gpus_amount) | `ONE`
[gpus_overcrowded_exist](./attributes.md#gpus_overcrowded_exist) | `True`

