#include <iostream>
#include <fstream>
#include <algorithm>
#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <unistd.h>
#include <pwd.h>
#include <limits.h>
#include <string.h>
#include <libgen.h>
#include <Python.h>

#ifndef SPATH
#error "The binary should be compiled with SPATH variable"
#endif

using namespace std;

// throw an error
static inline void error(string s, int errn){
  cout << s << endl;
  exit(errn);
}

// trim from start (in place)
static inline void ltrim(string &s) {
    s.erase(s.begin(), find_if(s.begin(), s.end(), [](int ch) {
        return !isspace(ch);
    }));
}

// trim from end (in place)
static inline void rtrim(string &s) {
    s.erase(find_if(s.rbegin(), s.rend(), [](int ch) {
        return !isspace(ch);
    }).base(), s.end());
}

// trim from both ends (in place)
static inline string trim(string &s) {
    ltrim(s);
    rtrim(s);
    return s;
}

int main(int argc, char **argv)
{
  string scriptfile = SPATH;

  if (trim(scriptfile).length() == 0) {
    error(string("SPATH cannot be empty. Recompile the binary"), 1);
  }
  
  // check the arguments for special characters
  for(int i=1; i<argc; i++) {
    if (string(argv[i]).find_first_not_of("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ01234567890-_ ") != std::string::npos){
      error(string("Invalid argument(not allowed characters): ") + argv[i], 3);
    }
  }

  wchar_t *program = Py_DecodeLocale(scriptfile.c_str(), NULL);
  FILE* fp;

  Py_SetProgramName(program);
  Py_Initialize();

  fp = _Py_fopen(scriptfile.c_str(), "r");
  if (fp == NULL) {
    error(string("Cannot open the script ") + scriptfile, 4);
  }

  wchar_t **pargv;

  pargv = new wchar_t* [argc];

  pargv[0] = program;
  for(int i=1; i<argc; i++) {
    pargv[i] = Py_DecodeLocale(argv[i], NULL);
  }

  PySys_SetArgv(argc, pargv);

  int pyres = PyRun_SimpleFile(fp, scriptfile.c_str());
  if (pyres < 0) {
    error(string("Error running the tool ") + scriptfile + " (error " + to_string(pyres) + ")", 5);
  }

  int fres = Py_FinalizeEx();
  if (fres < 0) {
    error(string("Error during finalizing (error ") + to_string(fres) + ")", 6);
  }

  return 0;
}
