# Aggregator
The tool aggregates data in a format which is suitable for generating reports.

*Currently also contains the script to export `jobinfo` data into DB from the post execution script*.

## Requirements

The aggregator needs particular software packages to be installed along with data available in DB.

-  Python 3.
-  InfluxDB. Currently the aggregator works only with InfluxDB, so it should be installed and the aggregator should be configured as shown in configuration section below.
-  DB should contain all required data specified in [DB spec](https://gibraltar.chi.uni-hannover.de/profit-hpc/ProfiT-HPC/blob/d9a21af233ab373bf90420e3f0f0c05e1c65aef8/Internals/DB/InfluxDBspec.md).

You should also install Python dependencies with:

```bash
python3 -m pip install -r requirements.txt
```

## Configuration

Samples for configurations can be stored in the repository, please rename corresponding templates with an extension `.py.sample` to `.py` and change placeholder values accordingly.

Real configs should be ignored in `.gitignore` file.

Sample configs must not be used in the code.

**Example**: `influxdb.py.sample` -> `influxdb.py`

## Security

The aggregator tool requires an access to the database (currently InlfuxDB). DB credentials are stored in a plain text file and accessible by anyone who has right to read configuration files. In order to prevent users from accessing DB credentials and job data that does not belong to them, the toolkit provides a setuid executable to run aggregator. In order to use this feature follow the instructions:

1. Configure aggregator to use setuid binary by setting `SECUSER=True` in `conf/config.py`.
2. Compile setuid binary: `cd setuid-runner; make`.
    - It requires Python3 to be installed in the system.
    - Configure the Makefile and set at least `SPATH` variable and `LDFLAGS` variable if needed.
3. You can move `setuid-runner/setuid-runner` anywhere where it is accessible by users
4. Change the ownership of all files to `safeuser` and remove the access for anyone else: `chown -R safeuser aggregator && chmod -R go-rwx aggregator`
5. Set **setuid** bit of the `setuid-runner` binary and ownership accordingly: `chmod ug+s,a+rx setuid-runner && chown safeuser setuid-runner`
6. Now in order to fetch the data call `setuid-runner` as you call `data.py`, for instance: `./setuid-runner -t text JOBID`

## Usage

The main executable of the aggregator module is `data.py`. You can type `./data.py -h` for more help.

```
usage: data.py [-h] [-t {text,pdf,all}] [-o [OUTPUT_DIR]] JOBID

Gets the job information required for generating text or PDF reports and
outputs it in JSON format.

positional arguments:
  JOBID                 job ID used in the batch system

optional arguments:
  -h, --help            show this help message and exit
  -t {text,pdf,all}, --type {text,pdf,all}
                        type of the output (default: text)
  -o [OUTPUT_DIR], --output-dir [OUTPUT_DIR]

                        output directory (default: None)
```

## Get test output

In order to get `json` output from the test data, located at `test/data`, a docker container with InfluxDB instance and imported data should be running. In order to build the docker container with necessary test data, run the following script:
```
test/docker/influxdb/build_influxdb.sh
```
To run the InfluxDB container, simply execute:
```
test/docker/influxdb/run_influxdb.sh
```
After the influxDB instance is up you need to configure the aggregator to use it, by copying `influxdb.sample` -> `influxdb.py` and editing it as:

```
IDB = {
    "username": "",
    "password": "",
    "database": "pfit",
    "api_url": "http://localhost:58086",
    "ssl": False,
}
```

If you are running tests on the machine without Slurm, then you should switch off the security mechanisms of Aggregator by setting corresponding option in the configuration file (./conf/config.py):

```
SECUSER = True
```

As an example the following command will output the test data in the `json` format for a `pdf` report:

```
  ./data.py -t pdf 1352076
```

You can use other `JOBID`s which you can find in `test/data` directory.

**Note**: Test data with JOBID `2368599` doesn't include *Infiniband* metrics, therefore it is required to switch off `InfluxDB` support in the configuration file(`conf/config.py`) before using it.

## Export job info

In order to export the job info with `JOBID` you should call `export.py`:
```bash
  export.py JOBID
```
Then the aggregator will gather a job information with ID `JOBID` from the batch system configured in `/conf/config.py` and save it into the configured database as a `pfit-jobinfo` and additionally in `pfit-jobinfo-alloc` measurements.

## Recommendation system

Currently the recommendation system is a module used by aggregator and located in this repository under [rcm](./rcm) directory. Please see the [documentations](./rcm/docs) and [README](./rcm/README.md) files for more information.
