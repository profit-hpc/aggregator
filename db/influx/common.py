# db/common.py common functions
import requests
import json
import sys
from requests.auth import HTTPBasicAuth
from conf import influxdb as confdb
from . import metrics

def fetch_data(query):
    payload = {'db': confdb.IDB["database"], 'q': query}
    query_url = "{:s}/query".format(confdb.IDB["api_url"])
    r = requests.post(query_url, verify=confdb.IDB["ssl"], auth=HTTPBasicAuth(
        confdb.IDB["username"], confdb.IDB["password"]), params=payload)

    # not only status code 200 gives us results, so we need to find out which are the bad/good status codes.
    if r.status_code != 200:
        raise RuntimeError(
            "Couldn't retrieve the data from InfluxDB. Code: {}, Response: {}".format(
                r.status_code, json.dumps(r.json()))
            )

    result = r.json()['results']

    return result

# format metric
def format_metric(metrics_dict, metric, value):
    type = metrics_dict[metric]["type"]

    return format_value(type, value)

def format_value(fmt, value):
    if value is None:
        return None

    if fmt is metrics.MType.INT:
        return int(float(value))
    if fmt is metrics.MType.STR:
        return str(value)
    if fmt is metrics.MType.FLT:
        return float(value)

    return None

def eprint(*args, err=1, **kwargs):
    print(*args, file=sys.stderr, **kwargs)
    exit(err)
