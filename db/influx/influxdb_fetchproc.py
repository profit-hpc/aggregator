# db/getinfluxdb.py gets metrics from the InfluxDB by jobID
# and returns parsed dictionary

from collections import defaultdict
import conf.config as conf
from . import common
from . import metrics

m_proc = conf.measurements["proc"]

def get_query_sum_of_max(metric, job_id, metric_db):
    aggr_outer = 'sum("{:s}_aggr") as "{:s}"'.format(metric, metric)
    aggr_inner = 'max("{:s}") as "{:s}_aggr"'.format(metric_db, metric)

    query = (
        'SELECT {:s} FROM ('
        'SELECT {:s}, "host" FROM "{:s}" '
        'WHERE "jobid1" = \'{:s}\' '
        'GROUP BY "pid") GROUP BY "host";'
    ).format(aggr_outer, aggr_inner, m_proc, job_id)

    return query


def get_query_sum_per_interval(metric, job_id, aggr, metric_db):
    interval = conf.METRIC_INTERVAL

    aggr_outer = '{:s}("{:s}_aggr") as "{:s}"'.format(aggr, metric, metric)
    aggr_inner = 'sum("{:s}") as "{:s}_aggr"'.format(metric_db, metric)

    query = (
        'SELECT {:s} FROM ('
        'SELECT {:s} '
        'FROM "{:s}" WHERE "jobid1" = \'{:s}\' '
        'GROUP BY "host", time({:d}s)) GROUP BY "host";'
    ).format(aggr_outer, aggr_inner, m_proc, job_id, interval)

    return query


def get_query_max_of_sum_per_interval(metric, job_id, metric_db):
    return get_query_sum_per_interval(metric, job_id, "max", metric_db)


def get_query_avg_of_sum_per_interval(metric, job_id, metric_db):
    return get_query_sum_per_interval(metric, job_id, "mean", metric_db)


def get_proc_queries(job_id):
    queries = ""

    for m, par in metrics.metrics_proc.items():
        if par["query"] is metrics.QType.SUM_OF_MAX:
            q = get_query_sum_of_max(m, job_id, par["dbname"])
        elif par["query"] is metrics.QType.MAX_SUM_PER_INT:
            q = get_query_max_of_sum_per_interval(m, job_id, par["dbname"])
        elif par["query"] is metrics.QType.AVG_SUM_PER_INT:
            q = get_query_avg_of_sum_per_interval(m, job_id, par["dbname"])
        else:
            raise ValueError('Unknown query type:', par["query"])

        queries += q

    return queries


def parse_proc_response(data, queries):
    nodes = defaultdict(dict)

    queries = queries.split(";")

    for i in range(0, len(data)):
        if "series" not in data[i]:
            if "statement_id" in data[i]:
                sid = data[i]["statement_id"]
                if len(queries) > sid:
                    if conf.DEBUG:
                        print("The query was not executed", queries[sid])
                else:
                    if conf.DEBUG:
                        print("Some queries were not executed", data[i])
            else:
                raise RuntimeError("Cannot parse the output", data[i])

            continue
            
        for s in range(0, len(data[i]['series'])):
            metric = data[i]['series'][s]['columns'][1]
            host = data[i]['series'][s]['tags']['host']
            nodes[host][metric] = data[i]['series'][s]['values'][0][1]

    return nodes

def format_data(procinfo_init):
    procinfo = defaultdict(dict)

    for node, vars in procinfo_init.items():
        for name, par in metrics.metrics_proc.items():
            raw_value = vars.get(name, None)
            value = common.format_metric(metrics.metrics_proc, name, raw_value)
            procinfo[node][name] = value

    return procinfo


def get_proc_data(job_id):
    query = get_proc_queries(job_id)

    procdata_raw = common.fetch_data(query)

    procdata_parsed = parse_proc_response(procdata_raw, query)

    procdata = format_data(procdata_parsed)

    return procdata
