RULES = [
    {
        "attrs": {
            "gpu_job": False,
            "job_nodes_amount": "MULT",
            "cpu_usage_total_max": "NORM",
            "overloaded_node_exists": True},
        "msg": "Some nodes are overloaded. Probably by other processes"},
    {
        "attrs": {
            "req_walltime": "HIGH"},
        "msg": "The requested walltime is too hight. Try to request less time"},
    {
        "attrs": {
            "gpu_job": False,
            "job_nodes_amount": "MULT",
            "req_walltime": "NORM",
            "cpu_usage_total_max": "LOW",
            "node_cpu_usage_min": "LOW",
            "node_cpu_usage_max": "NORM"},
        "msg": "The CPU usage on some nodes is low, please adjust the amount of requested resources or workload of the job"},
    {
        "attrs": {
            "job_nodes_amount": "MULT",
            "req_walltime": "NORM",
            "node_cpu_usage_min": "ZERO",
            "node_cpu_usage_max": "NORM",
            "gpu_usage_min": "ZERO"},
        "msg": "Some nodes were not used during the runtime"},
    {
        "attrs": {
            "gpu_job": False,
            "job_nodes_amount": "MULT",
            "req_walltime": "NORM",
            "cpu_usage_total_max": "NORM",
            "node_cpu_usage_min": "LOW",
            "node_cpu_usage_max": "NORM"},
        "msg": "The CPU usage is not distributed equally among the nodes. Try to use the nodes evenly"},
    {
        "attrs": {
            "gpu_job": False,
            "job_nodes_amount": "MULT",
            "req_walltime": "NORM",
            "mem_usage_total": "LOW",
            "cpu_usage_total_max": "LOW",
            "node_cpu_usage_min": "LOW",
            "node_cpu_usage_max": "LOW"},
        "msg": "The CPU usage of the job is low on all nodes, please request appropriate amount of resources"},
    {
        "attrs": {
            "gpu_job": False,
            "job_nodes_amount": "MULT",
            "req_walltime": "NORM",
            "mem_usage_total": "NORM",
            "cpu_usage_total_max": "LOW",
            "node_cpu_usage_min": "LOW",
            "node_cpu_usage_max": "LOW"},
        "msg": "The CPU usage of the job is low on all nodes. Please check if the job is running properly"},
    {
        "attrs": {
            "gpu_job": False,
            "job_nodes_amount": "ONE",
            "req_walltime": "NORM",
            "node_cpu_usage_max": "LOW"},
        "msg": "The CPU usage of the node is low. It might indicate that the job is not running in full power"},
    {
        "attrs": {
            "job_nodes_amount": "ONE",
            "req_walltime": "NORM",
            "node_cpu_usage_max": "NORM",
            "overloaded_node_exists": True},
        "msg": "The node is overloaded. Probably by other processes on the node"},
    {
        "attrs": {
            "job_nodes_amount": "ONE",
            "req_walltime": "NORM",
            "node_cpu_usage_max": "HIGH",
            "overloaded_node_exists": True},
        "msg": "The node is overloaded and cpu usage of job was high"},
    {
        "attrs": {
            "job_nodes_amount": "MULT",
            "mem_swap_used": True},
        "msg": "Swap was used on one of the nodes"},
    {
        "attrs": {
            "job_nodes_amount": "ONE",
            "mem_swap_used": True},
        "msg": "Swap was used on the node"},
    {
        "attrs": {
            "gpu_job": True,
            "gpus_amount": "ONE",
            "gpu_usage_max": "LOW"},
        "msg": "The single GPU has low usage, which might be caused by misconfiguration of the application/job"},
    {
        "attrs": {
            "gpu_job": True,
            "gpus_amount": "MULT",
            "gpu_usage_max": "LOW"},
        "msg": "All GPUs have low usage, which might be caused by misconfiguration of the application/job"},
    {
        "attrs": {
            "gpu_job": True,
            "gpus_amount": "MULT",
            "gpu_usage_max": ["NORM", "HIGH"],
            "gpu_usage_min": "LOW"},
        "msg": "Some of the GPUs have low usage which might be caused by workload inbalance"},
    {
        "attrs": {
            "gpu_job": True,
            "gpus_amount": "MULT",
            "gpu_usage_min": "ZERO"},
        "msg": "Some of the GPUs were not used, which might be caused by misconfiguration of the application/job"},
    {
        "attrs": {
            "gpu_job": True,
            "gpus_amount": "MULT",
            "gpu_usage_max": "ZERO"},
        "msg": "None of the GPUs were used, which might be caused by misconfiguration of the application/job"},
    {
        "attrs": {
            "gpu_job": True,
            "gpus_amount": "MULT",
            "gpus_overcrowded_exist": True},
        "msg": "Some GPUs have more than 1 process using them at the same time. Check the balance between CPU processes and amount of used GPUs"},
    {
        "attrs": {
            "gpu_job": True,
            "gpus_amount": "ONE",
            "gpus_overcrowded_exist": True},
        "msg": "The GPU has been used by more than 1 process simultaneously. Check the balance between CPU processes and amount of used GPUs"},
]
