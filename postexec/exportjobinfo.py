import subprocess
import csv
import re
import time
from datetime import datetime
from io import StringIO
import requests
from requests.auth import HTTPBasicAuth
from db.influx import common
from db.influx.metrics import MType
import conf.config as conf
import conf.influxdb as confdb

_info = {
    "user_name":        {"rename": "user_name",
                         "type": MType.STR,
                         "lsf": "USER",
                         "slurm": "User"},
    "used_queue":       {"rename": "used_queue",
                         "type": MType.STR,
                         "lsf": "QUEUE",
                         "slurm": "Partition"},
    "submit_time":      {"rename": "submit_time",
                         "type": MType.INT,
                         "lsf": "SUBMIT_TIME",
                         "slurm": "Submit"},
    "start_time":       {"rename": "start_time",
                         "type": MType.INT,
                         "lsf": "START_TIME",
                         "slurm": "Start"},
    "end_time":         {"rename": "end_time",
                         "type": MType.INT,
                         "lsf": "FINISH_TIME",
                         "slurm": "End"},
    "run_time":         {"rename": "run_time",
                         "type": MType.INT,
                         "lsf": "RUN_TIME",
                         "slurm": "Elapsed",
                         "regex": "([0-9]+)"},
    "exit_code":        {"rename": "exit_code",
                         "type": MType.INT,
                         "lsf": "EXIT_CODE",
                         "slurm": "ExitCode",
                         "regex": "([0-9]+):"},
    "requested_time":   {"rename": "requested_time",
                         "type": MType.INT,
                         "lsf": "RUNTIMELIMIT",
                         "slurm": "Timelimit",
                         "regex": r"([0-9]+\.[0-9]+)"},
    "requested_cu":     {"rename": "requested_cu",
                         "type": MType.INT,
                         "lsf": "SLOTS",
                         "slurm": "NCPUS"},
    "num_nodes":        {"rename": "num_nodes",
                         "type": MType.INT,
                         "lsf": "NEXEC_HOST",
                         "slurm": "NNodes"},
    "alloc_slots":      {"rename": "alloc_cu",
                         "type": MType.STR,
                         "lsf": "ALLOC_SLOT",
                         "slurm": "NodeList"},
}


def get_by_regex(v, regex):
    m = re.search(regex, v)

    res = None
    if m is not None:
        res = m.group(1)

    return res


def date_to_timestamp(v, format):
    if v is None:
        return None

    raw_r = datetime.strptime(v, format)
    dt = raw_r.replace(year=datetime.today().year).timetuple()
    ts = time.mktime(dt)

    return ts


def parse_job_info_lsf(raw_data):
    time_fields = ["submit_time", "start_time", "end_time"]
    reg_fields = {
        "run_time": "([0-9]+)",
        "requested_time": r"([0-9]+\.[0-9]+)",
    }

    job_info = {}
    for m, par in _info.items():
        if par["lsf"] not in raw_data:
            raise ValueError("Couln't parse value", par["lsf"])
        job_info[m] = raw_data[par["lsf"]]

    # prepare raw data

    # dates to timestamp
    for f in time_fields:
        d = get_by_regex(job_info[f], "([A-Za-z]+ +[0-9]+ [0-9:]+)")
        job_info[f] = date_to_timestamp(d, "%b %d %H:%M")

    # regexes
    for f, r in reg_fields.items():
        job_info[f] = get_by_regex(job_info[f], r)

    # reformat alloc info

    alloc_info = {}

    nodes_raw = raw_data[job_info["alloc_slots"]].split(":")
    for n in nodes_raw:
        cu, node_id = n.split("*")
        alloc_info[node_id] = {"cpu": int(cu), "mem": 0}

    # format values
    for m, par in _info.items():
        job_info[m] = common.format_metric(_info, m, job_info[m])

    # reevaluation

    # requested time from min to sec
    if job_info["requested_time"] is not None:
        job_info["requested_time"] *= 60

    return job_info, alloc_info


def slurm_time_to_sec(slurm_time):
    if slurm_time is None:
        return None

    # 2:days, 4:hours, 5:minutes, 6:seconds
    m = re.search(
        "(([0-9]{1,2})-)?(([0-9]{2}):)?([0-9]{2}):([0-9]{2})", slurm_time)

    if m.group(5) is None or m.group(6) is None:
        raise ValueError("Couldn't parse time", slurm_time)

    secs = int(m.group(5)) * 60 + int(m.group(6))

    if m.group(4) is not None:
        secs += int(m.group(4)) * 3600

    if m.group(2) is not None:
        secs += int(m.group(2)) * 86400

    return secs


def slurm_format_alloc(nodes_info):
    formatted = ":".join("{}*{}*{}".format(nodes_info[n]["cpu"], nodes_info[n]["mem"], n) for n in nodes_info)

    return formatted

def slurm_parse_nodes_info(nodes):
    slurm_list_set = [",","[","-"]
    is_list = False
    for c in slurm_list_set:
        if c in nodes:
            is_list = True
            break

    if is_list:
        tolist_comm = "scontrol show hostname {:s}".format(nodes)
        result = subprocess.run(tolist_comm, stdout=subprocess.PIPE, shell=True,
                                executable='/bin/bash')
        out = result.stdout.decode("utf-8")
        out = out.splitlines()
    else:
        out = [nodes]

    ret = out

    return ret

def slurm_parse_cpu_info(cpus):
    cpus_list = cpus.split(",")
    cpu_count = 0
    for cpu_info in cpus_list:
        if "-" in cpu_info:
            a_and_b = cpu_info.split("-")
            cpu_count += int(a_and_b[1]) - int(a_and_b[0]) + 1
        else:
            cpu_count += 1

    return cpu_count


def slurm_fetch_alloc_info(job_id):
    fetch_comm = "scontrol show job -d {:s}".format(job_id)
    result = subprocess.run(fetch_comm, stdout=subprocess.PIPE, shell=True,
                            executable='/bin/bash')
    out = result.stdout.decode("utf-8")

    alloc_raw = re.findall("Nodes=([^ ]+) +CPU_IDs=([^ ]+) +Mem=([ 0-9]+)", out)
    allocs = {}

    if len(alloc_raw) == 0:
        raise ValueError("Cannot parse Slurm allocation info", out)

    for (nodes_list_raw, cpu_list_raw, mem_alloc_raw) in alloc_raw:
        nodes_list = slurm_parse_nodes_info(nodes_list_raw)
        cpu_count = slurm_parse_cpu_info(cpu_list_raw)
        if re.match(r"[0-9]+", mem_alloc_raw):
            mem_alloc = int(mem_alloc_raw)
        else:
            mem_alloc = ""

        for node_name in nodes_list:
            allocs[node_name] = {"cpu": cpu_count, "mem": mem_alloc}

    return allocs

def parse_job_info_slurm(raw_data, alloc_info):
    time_fields = ["submit_time", "start_time", "end_time"]
    dur_fields = ["run_time", "requested_time"]
    reg_fields = ["exit_code"]

    job_info = {}
    for m, par in _info.items():
        if par["slurm"] not in raw_data:
            raise ValueError("Couln't parse value", par["slurm"])
        job_info[m] = raw_data[par["slurm"]]

    # prepare raw data

    # dates to timestamp
    for f in time_fields:
        if f == "end_time" and job_info[f] == "Unknown":
          job_info[f] = 0
        else:
          job_info[f] = date_to_timestamp(job_info[f], "%Y-%m-%dT%H:%M:%S")

    # regexes
    for f in reg_fields:
        job_info[f] = get_by_regex(job_info[f], _info[f]["regex"])

    # slurm times to seconds
    for f in dur_fields:
        job_info[f] = slurm_time_to_sec(job_info[f])

    # reformat cu allocation
    job_info["alloc_slots"] = slurm_format_alloc(alloc_info)

    # format values
    for m, par in _info.items():
        job_info[m] = common.format_metric(_info, m, job_info[m])

    return job_info


def format_fields(m, val, metrics):
    if metrics[m]["type"] == MType.STR:
        val = "\"{:s}\"".format(val)
    return "{}={}".format(_info[m]["rename"], val)


def format_influxdb_alloc(job_id, alloc_info, cur_time):
    mname = conf.job_info["measurement_name"] + "-alloc"

    req = ""
    for node_name, alloc_data in alloc_info.items():
        alloc_cu = alloc_data["cpu"]
        alloc_mem = alloc_data["mem"]
        req += "{:s},jobid={:s},host={:s} ".format(mname, job_id, node_name)

        req += "alloc_cu={},".format(alloc_cu)
        req += "alloc_mem={}".format(alloc_mem)

        req += " {:d}\n".format(cur_time)

    return req

def format_influx_db_out(job_id, job_data, alloc_data):
    cur_time = int(time.time())
    mname = conf.job_info["measurement_name"]

    req = "{:s},jobid={:s},uname={:s} ".format(mname, job_id, job_data["user_name"])

    req += ",".join(format_fields(k, v, _info) for k, v in job_data.items())

    req += " {:d}\n".format(cur_time)

    alloc_influx = format_influxdb_alloc(job_id, alloc_data, cur_time)

    req += alloc_influx

    return req

def format_format_str(batch, qdelim):
    oformat = qdelim.join(val[batch] for key, val in _info.items())

    if conf.BATCH_SYSTEM == "LSF":
        oformat += " delimiter=','"

    return oformat


def fetch_job_data_general(job_id, batch, qdelim, rdelim):
    fetch_comm_temp = conf.job_info["fetch_job_info_{:s}".format(batch)]

    oformat = format_format_str(batch, qdelim)

    fetch_comm = fetch_comm_temp.format(oformat, job_id)
    result = subprocess.run(fetch_comm, stdout=subprocess.PIPE, shell=True,
                            executable='/bin/bash')

    out = result.stdout.decode("utf-8")

    f = StringIO(out)
    reader = csv.DictReader(f, delimiter=rdelim)

    raw_data = next(reader)

    return raw_data


def fetch_job_data(job_id):
    batchsys = conf.BATCH_SYSTEM
    if batchsys == "LSF":
        raw_info = fetch_job_data_general(job_id, "lsf", " ", ",")
        job_info, node_info = parse_job_info_lsf(raw_info)
    elif batchsys == "SLURM":
        raw_info = fetch_job_data_general(job_id, "slurm", ",", "|")
        node_info = slurm_fetch_alloc_info(job_id)
        job_info = parse_job_info_slurm(raw_info, node_info)
    else:
        raise ValueError("Configured batch system is not supported")

    return job_info, node_info

def export_job_info(job_id):
    job_info, alloc_info = fetch_job_data(job_id)

    fdata = format_influx_db_out(job_id, job_info, alloc_info)
    payload = {'db': confdb.IDB["database"], 'precision': 's'}

    write_url = "{:s}/write".format(confdb.IDB["api_url"])
    r = requests.post(write_url, auth=HTTPBasicAuth(
        confdb.IDB["username"], confdb.IDB["password"]),
        params=payload, data=fdata)

    if r.status_code != 204:
        raise ValueError("Couldn't write to InfluxDB", r.status_code, r.content)

    return True
