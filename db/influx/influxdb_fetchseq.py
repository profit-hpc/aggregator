# db/getinfluxdb.py gets metrics from the InfluxDB by jobID
# and returns parsed dictionary

import math
from collections import defaultdict
import conf.config as conf
from . import common
from . import metrics

DB_INTERVAL = conf.METRIC_INTERVAL

def get_query_aggr_per_node_for_job(aggr, metric, job_id, metric_db, measurement, delta, t_start, t_end):
    aggr = '{:s}("{:s}") as "{:s}"'.format(aggr, metric_db, metric)

    query = (
        'SELECT mean({met}) as "{met}" FROM ('
        'SELECT {agr} FROM "{mes}" '
        'WHERE "jobid1" = \'{job}\' '
        'AND time >= {start}s AND time <= {end}s '
        'GROUP BY "host", time({dbdelta}s)) '
        'WHERE time >= {start}s AND time <= {end}s '
        'GROUP BY "host", time({delta}s);'
    ).format(agr = aggr, mes = measurement, job = job_id,
        start=t_start, end=t_end, delta=delta, met = metric,
        dbdelta = DB_INTERVAL)

    return query

def get_query_aggr_for_nodes_double_select(gaggr, aggr, metric, nodes, metric_db, measurement, delta, t_start, t_end):
    aggrf = '{:s}("{:s}") as "{:s}"'.format(aggr, metric_db, metric)

    node_cond = ' OR '.join("host='{:s}'".format(n) for n in nodes)

    query = (
        'SELECT {gagr}({met}) as "{met}" FROM ('
        'SELECT {agr} FROM "{mes}" '
        'WHERE ({nodes}) '
        'AND time >= {start}s AND time <= {end}s '
        'GROUP BY "host", time({dbdelta}s)) '
        'WHERE time >= {start}s AND time <= {end}s '
        'GROUP BY "host", time({delta}s);'
    ).format(gagr = gaggr, agr = aggrf, mes = measurement, nodes = node_cond,
        start = t_start, end = t_end, delta = delta, met = metric,
        dbdelta = DB_INTERVAL)

    return query

def get_query_aggr_for_nodes(aggr, metric, nodes, metric_db, measurement, delta, t_start, t_end):
    aggrf = '{:s}("{:s}") as "{:s}"'.format(aggr, metric_db, metric)

    node_cond = ' OR '.join("host='{:s}'".format(n) for n in nodes)

    query = (
        'SELECT {agr} FROM "{mes}" '
        'WHERE ({nodes}) '
        'AND time >= {start}s AND time <= {end}s '
        'GROUP BY "host", time({delta}s);'
    ).format(agr = aggrf, mes = measurement, nodes = node_cond,
        start = t_start, end = t_end, delta = delta, met = metric)

    return query


def get_query_sum_per_node_of_job(metric, job_id, metric_db, measurement, delta, t_start, t_end):
    return get_query_aggr_per_node_for_job("sum", metric, job_id, metric_db, measurement, delta, t_start, t_end)


def get_query_avg_of_node(metric, nodes, metric_db, measurement, delta, t_start, t_end):
    return get_query_aggr_for_nodes("mean", metric, nodes, metric_db, measurement, delta, t_start, t_end)

def get_query_sum_of_node(metric, nodes, metric_db, measurement, delta, t_start, t_end):
    return get_query_aggr_for_nodes("sum", metric, nodes, metric_db, measurement, delta, t_start, t_end)

def get_query_max_of_node(metric, nodes, metric_db, measurement, delta, t_start, t_end):
    return get_query_aggr_for_nodes("max", metric, nodes, metric_db, measurement, delta, t_start, t_end)


def get_seq_queries(job_id, t_start, t_end, delta, nodes):
    queries = ""

    for m, par in metrics.metrics_seq.items():
        # skip generating infiniband queries if not activated
        if par["measurement"] == conf.measurements["infiniband"] and conf.INFINIBAND is False:
            continue

        if par["query"] is metrics.QType.SUM_PER_NODE:
            q = get_query_sum_per_node_of_job(
                m, job_id, par["dbname"], par["measurement"], delta, t_start, t_end)
        elif par["query"] is metrics.QType.AVG_OF_NODE:
            q = get_query_avg_of_node(
                m, nodes, par["dbname"], par["measurement"], delta, t_start, t_end)
        elif par["query"] is metrics.QType.MAX_OF_NODE:
            q = get_query_max_of_node(
                m, nodes, par["dbname"], par["measurement"], delta, t_start, t_end)
        elif par["query"] is metrics.QType.SUM_OF_NODE:
            q = get_query_sum_of_node(
                m, nodes, par["dbname"], par["measurement"], delta, t_start, t_end)
        else:
            raise ValueError("Unknown query type: {:s}".format(par["query"]))

        queries += q

    return queries


def format_seq_val(metric, val):
    if metric not in metrics.metrics_seq:
        return val

    fmtd = common.format_value(metrics.metrics_seq[metric]["type"], val)

    return fmtd


def parse_seq_response(data, queries):
    nodes = defaultdict(dict)

    queries = queries.split(";")

    for i in range(0, len(data)):
        if "series" not in data[i]:
            if "statement_id" in data[i]:
                sid = data[i]["statement_id"]
                if len(queries) > sid:
                    if conf.DEBUG:
                        print("The query was not executed", queries[sid])
                else:
                    if conf.DEBUG:
                        print("Some queries were not executed", data[i])
            else:
                raise RuntimeError("Cannot parse the output", data[i])

            continue
            
        for s in range(0, len(data[i]['series'])):
            serie = data[i]['series'][s]
            metric = serie['columns'][1]
            host = serie['tags']['host']
            nodes[host][metric] = []
            for v in range(0, len(serie['values'])):
                nodes[host][metric].append(
                    format_seq_val(metric, serie['values'][v][1]))

    return nodes


def format_data(seqinfo_init):
    seqinfo = defaultdict(dict)

    for node, metric in seqinfo_init.items():
        if metric not in metrics.metrics_seq:
            continue

        for val, vkey in enumerate(seqinfo_init[metric]):
            value = common.format_value(metrics.metrics_seq[metric], val)
            seqinfo[node][name][vkey] = value

    return seqinfo


def get_seq_data(job_id, t_start, t_end, nodes):
    delta = math.ceil((t_end - t_start) / conf.SEQ_MAX_POINTS)
    delta = max(delta, conf.METRIC_INTERVAL)

    query = get_seq_queries(job_id, t_start, t_end, delta, nodes)
    if conf.DEBUG:
        print(query)

    seqdata_raw = common.fetch_data(query)

    seqdata_parsed = parse_seq_response(seqdata_raw, query)

    if conf.DEBUG:
        for host in seqdata_parsed:
            for metric in seqdata_parsed[host]:
                print(host, metric, len(seqdata_parsed[host][metric]), str(
                    seqdata_parsed[host][metric])[:100])


    return seqdata_parsed, delta
