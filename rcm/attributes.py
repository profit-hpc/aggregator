# this file contains attributs wich are used to build rules
# the attributes can be build using the instance of Aggregator class.
# each attribute should have finite number of values

from db.aggrstruct import *
from . import helpers

DEBUG = False

def printd(*args, **kargs):
    if DEBUG:
        print(*args, **kargs)

def rcmattr(func):
    '''Function decorator. Checks and sets arguments'''
    def wrapper(*args, **kwargs):
        printd("-- evaluation of the attribute {:s} --".format(func.__name__))
        res = func(*args, **kwargs)
        printd("   attribute {:s} = {}".format(func.__name__, res))
        return res

    wrapper.decorator = rcmattr
    wrapper.__name__ = func.__name__
    wrapper.__doc__ = func.__doc__

    return wrapper

class Attributes:
    _aggr = None
    attr_values = {}

    def __init__(self, Aggr):
        # init function
        if type(Aggr) != Aggregator:
            raise TypeError("argument for init should be Aggregator")
        self._aggr = Aggr
        return

    def calculate_attrs(self):
        attr_func = self.get_attribute_funcs()

        for func in attr_func:
            self.attr_values[func.__name__] = func()

        return self.attr_values

    def get_attribute_funcs(self):
        """
            Returns all methods with attribute decorator
        """
        methods = []
        for maybeDecorated in dir(self):
            cur_attr = self.__getattribute__(maybeDecorated)
            if hasattr(cur_attr, 'decorator'):
                if cur_attr.decorator is rcmattr:
                    methods.append(cur_attr)
        return methods


    @rcmattr
    def cpu_usage_total_max(self):
        cpu_usage = 0
        cpu_usage_sum = 0

        alloc_cu_sum = 0

        cpu_requested = self._aggr.job.requested_cu

        for node in self._aggr.nodes:
            thr_cnt = node.virt_thr_core + node.phys_thr_core

            cpu_usage_sum += node.proc.cpu_usage
            alloc_cu_sum += node.alloc_cu / thr_cnt

        cpu_usage = cpu_usage_sum / cpu_requested

        printd("{} = {} (total cpu_usage) / {} (requested cores)".
            format(cpu_usage, cpu_usage_sum, cpu_requested))

        res = "NORM"
        if cpu_usage < 50 and alloc_cu_sum <= 8:
            res = "LOW"
        elif cpu_usage < 80 and alloc_cu_sum > 8:
            res = "LOW"

        return res

    @rcmattr
    def mem_usage_total(self):
        MEM_LOW_BOUND = 32*1024*1024*1024 # 32 GiB
        mem_req_total = 0
        mem_used_max = 0

        for node in self._aggr.nodes:
            thr_cnt = node.virt_thr_core + node.phys_thr_core
            mem_per_core = node.main_mem / (node.sockets * node.cores_per_socket * thr_cnt)

            mem_req_total += mem_per_core * node.alloc_cu
            mem_used_max += node.proc.mem_rss_max

        mem_ratio = mem_used_max / mem_req_total

        printd("mem total = {}, mem used {}, ratio {}".
            format(mem_req_total, mem_used_max, mem_ratio))

        res = "NORM"
        if mem_used_max > MEM_LOW_BOUND and mem_ratio > 1:
            res = "HIGH"
        elif mem_used_max > MEM_LOW_BOUND and mem_ratio < 0.3:
            res = "LOW"

        return res

    @rcmattr
    def node_cpu_usage_max(self):
        res = ["ZERO", "LOW", "NORM", "HIGH"]
        max_res = res.index("ZERO")
        for node in self._aggr.nodes:
            thr_cnt = node.virt_thr_core + node.phys_thr_core
            node_proc_ratio = node.proc.cpu_usage / (node.alloc_cu / thr_cnt)

            node_res = 0
            if node_proc_ratio == 0:
                node_res = res.index("ZERO")
            elif node_proc_ratio > 100:
                nodre_res = res.index("HIGH")
            elif node_proc_ratio < 50 and node.alloc_cu <= 8:
                node_res = res.index("LOW")
            elif node_proc_ratio < 80 and node.alloc_cu > 8:
                node_res = res.index("LOW")
            elif node_proc_ratio > 50 and node.alloc_cu <= 8:
                node_res = res.index("NORM")
            elif node_proc_ratio > 80 and node.alloc_cu > 8:
                node_res = res.index("NORM")

            max_res = max(node_res, max_res)

        return res[max_res]

    @rcmattr
    def node_cpu_usage_min(self):
        res = ["ZERO", "LOW", "NORM", "HIGH"]
        min_res = res.index("HIGH")
        for node in self._aggr.nodes:
            thr_cnt = node.virt_thr_core + node.phys_thr_core
            node_proc_ratio = node.proc.cpu_usage / (node.alloc_cu / thr_cnt)

            node_res = 0
            if node_proc_ratio == 0:
                node_res = res.index("ZERO")
            elif node_proc_ratio > 100:
                nodre_res = res.index("HIGH")
            elif node_proc_ratio < 50 and node.alloc_cu <= 8:
                node_res = res.index("LOW")
            elif node_proc_ratio < 80 and node.alloc_cu > 8:
                node_res = res.index("LOW")
            elif node_proc_ratio > 50 and node.alloc_cu <= 8:
                node_res = res.index("NORM")
            elif node_proc_ratio > 80 and node.alloc_cu > 8:
                node_res = res.index("NORM")

            printd("{}: {} / ({} / {}) = {}({})".format(node.name,
                node.proc.cpu_usage, node.alloc_cu,
                thr_cnt, node_proc_ratio, node_res))
            min_res = min(node_res, min_res)

        return res[min_res]

    @rcmattr
    def req_walltime(self):
        MIN_R = 4
        SHIFT_R = 6

        utime = self._aggr.job.run_time / 60 / 60
        rtime = self._aggr.job.requested_time / 60 / 60

        res = "UNKNOWN"
        if rtime > MIN_R and utime < rtime/2 and utime < (rtime - SHIFT_R):
            res = "HIGH"
        else:
            res = "NORM"

        return res

    @rcmattr
    def overloaded_node_exists(self):
        exists = False
        for node in self._aggr.nodes:
            if node.proc.cpu_usage > 90:
                continue

            if helpers.node_has_high_load_interval(node):
                exists = True
                break

        return exists

    @rcmattr
    def job_nodes_amount(self):
        nnodes = len(self._aggr.nodes)

        res = "ERR"
        if nnodes == 1:
            res = "ONE"
        elif nnodes > 1:
            res = "MULT"

        return res

    @rcmattr
    def mem_swap_used(self):
        swap_used = False

        for node in self._aggr.nodes:
            if node.proc.mem_swap_max > 0:
                swap_used = True

        return swap_used

    @rcmattr
    def gpu_job(self):
        is_gpu = False

        for node in self._aggr.nodes:
            if node.gpus:
                is_gpu = True

        return is_gpu

    @rcmattr
    def gpu_usage_max(self):
        res = ["ZERO", "LOW", "NORM", "HIGH"]
        max_res = res.index("ZERO")
        for node in self._aggr.nodes:
            if not node.gpus: continue

            for gpu in node.gpus:
                gpu_usage = gpu.usage_max

                if gpu_usage > 90:
                    gpu_res = res.index("HIGH")
                elif gpu_usage > 50:
                    gpu_res = res.index("NORM")
                elif gpu_usage > 0.5:
                    gpu_res = res.index("LOW")
                else:
                    gpu_res = res.index("ZERO")

                max_res = max(gpu_res, max_res)

        return res[max_res]

    @rcmattr
    def gpu_usage_min(self):
        res = ["ZERO", "LOW", "NORM", "HIGH"]
        min_res = res.index("HIGH")
        gpu_was_used = False

        for node in self._aggr.nodes:
            if not node.gpus: continue

            gpu_was_used = True

            for gpu in node.gpus:
                gpu_usage = gpu.usage_max

                if gpu_usage > 90:
                    gpu_res = res.index("HIGH")
                elif gpu_usage > 50:
                    gpu_res = res.index("NORM")
                elif gpu_usage > 0.5:
                    gpu_res = res.index("LOW")
                else:
                    gpu_res = res.index("ZERO")

                min_res = min(gpu_res, min_res)

        if not gpu_was_used:
            min_res = res.index("ZERO")
            
        return res[min_res]

    @rcmattr
    def gpus_amount(self):
        ngpus = 0
        for node in self._aggr.nodes:
            if node.gpus:
                ngpus += len(node.gpus)

        res = "ERR"
        if ngpus == 1:
            res = "ONE"
        elif ngpus > 1:
            res = "MULT"

        return res

    @rcmattr
    def gpus_overcrowded_exist(self):
        has_overcrowded = False

        for node in self._aggr.nodes:
            if not node.gpus: continue

            for gpu in node.gpus:
                if helpers.gpu_has_overcrowded_interval(gpu) is True:
                    has_overcrowded = True
                    break

            if has_overcrowded:
                break

        return has_overcrowded
