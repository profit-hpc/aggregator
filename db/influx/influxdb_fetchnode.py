# db/getnode.py gets node information directly from the nodes and saves locally
# for further usage

import conf.config as conf
from collections import defaultdict
from . import common
from . import metrics

m_node = conf.measurements["node"]

def get_nodeinfo_query(node_id, t_start, t_end):
    random_field = metrics.metrics_node["sockets"]["dbname"]
    query = (
        'SELECT last({:s}), * FROM "{:s}" '
        'WHERE time >= {:d}s AND time <= {:d}s AND '
        '"host" = \'{:s}\' GROUP BY "host";'
    ).format(random_field, m_node, t_start, t_end, node_id)
    return query

def get_query_max_of_node(node_id, msrmnt, metric, metric_db, t_start, t_end):
    query = (
        'SELECT max({:s}) as "{:s}" FROM "{:s}" '
        'WHERE time >= {:d}s AND time <= {:d}s AND '
        '"host" = \'{:s}\' GROUP BY "host";'
    ).format(metric_db, metric, msrmnt, t_start, t_end, node_id)

    return query

def get_query_sum_of_node(node_id, msrmnt, metric, metric_db, t_start, t_end):
    query = (
        'SELECT sum({:s}) as "{:s}" FROM "{:s}" '
        'WHERE time >= {:d}s AND time <= {:d}s AND '
        '"host" = \'{:s}\' GROUP BY "host";'
    ).format(metric_db, metric, msrmnt, t_start, t_end, node_id)

    return query

def get_node_queries(nodes, t_start, t_end):
    queries = ""

    for n in nodes:
        q = get_nodeinfo_query(n, t_start, t_end)
        queries += q

        for m, par in metrics.metrics_node.items():
            if par["measurement"] == conf.measurements["infiniband"] and conf.INFINIBAND is False:
                continue
            if par["measurement"] == conf.measurements["beegfs"] and conf.BEEGFS is False:
                continue

            if par["query"] is metrics.QType.MAX_OF_NODE:
                q = get_query_max_of_node(n, par["measurement"], m, par["dbname"], t_start, t_end)
                queries += q

    return queries

def parse_node_response(data, queries):
    nodes = defaultdict(dict)

    queries = queries.split(";")

    for i in range(0, len(data)):
        if "series" not in data[i]:
            if "statement_id" in data[i]:
                sid = data[i]["statement_id"]
                if len(queries) > sid:
                    if conf.DEBUG:
                        print("The query was not executed", queries[sid])
                else:
                    if conf.DEBUG:
                        print("Some queries were not executed", data[i])
            else:
                raise RuntimeError("Cannot parse the output", data[i])

            continue

        for s in range(0, len(data[i]['series'])):
            for m in range(0, len(data[i]['series'][0]['columns'])):
                host = data[i]['series'][s]['tags']['host']
                metric = data[i]['series'][s]['columns'][m]
                value = data[i]['series'][s]['values'][0][m]
                nodes[host][metric] = value

    return nodes

def format_node_info(nodeinfo_init):
    nodeinfo = defaultdict(dict)

    for n in nodeinfo_init:
        for name, par in metrics.metrics_node.items():
            raw_value = nodeinfo_init[n].get(par["dbname"], nodeinfo_init[n].get(name, None))
            value = common.format_metric(metrics.metrics_node, name, raw_value)
            nodeinfo[n][name] = value

    return nodeinfo

def get_node_data(nodes, t_start, t_end):
    nodes = set(nodes)

    queries = get_node_queries(nodes, t_start, t_end)

    nodedata_raw = common.fetch_data(queries)

    nodedata_parsed = parse_node_response(nodedata_raw, queries)

    nodeinfo = format_node_info(nodedata_parsed)

    return nodeinfo
