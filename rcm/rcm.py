from .rules import RULES
from rcm import attributes

attributes.DEBUG = False

def attributes_match(attrs, values):
    matches = True
    for attr_name, attr_expected in attrs.items():
        if attr_name not in values:
            raise ValueError("Attribute is not found", attr_name)

        attr_value = values[attr_name]
        expected_vals = []

        if isinstance(attr_expected, list):
            expected_vals.extend(attr_expected)
        else:
            expected_vals.append(attr_expected)

        if attr_value not in expected_vals:
            matches = False
            break

    return matches


def get_recommendations(Aggr):
    Attrs = attributes.Attributes(Aggr)

    attr_values = Attrs.calculate_attrs()

    rcms = []
    for rule in RULES:
        if attributes_match(rule["attrs"], attr_values):
            rcms.append(rule["msg"])

    return rcms
