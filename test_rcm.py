#! /usr/bin/env python3

import db.aggregator as aggregator
from rcm import rcm

aggr = aggregator.get_aggregator("1352076", "pdf")

rcms = rcm.get_recommendations(aggr)

print(rcms)
