#! /usr/bin/env python3

import json
import argparse
import postexec.exportjobinfo as xjob
import conf.config as conf

def main():
    parser = argparse.ArgumentParser(description="""
        Gets the job information from the Batch system and
        stores the job info in DB.
    """, formatter_class=argparse.ArgumentDefaultsHelpFormatter)

    parser.add_argument("JOBID",
                        help="job ID used in the batch system")

    args = parser.parse_args()
    job_id = args.JOBID

    res = xjob.export_job_info(job_id)
    if res is not True:
        return res

    print("Job {} was exported".format(job_id))
    return 0

if __name__ == "__main__":
    main()
