#!/bin/bash

#module load python/3.6.3 ## here you can load the module of environment for Python

TEXT_REPORT_LOC="../text-report/runme.py"
PDF_REPORT_LOC="../pdf-report/pdfgen/pfit_pdfreport.py"
SUID_BIN_LOC="setuid-runner/setuid-runner"

BTEXT="${TEXT_REPORT_LOC}"
BPDF="${PDF_REPORT_LOC}"
BDATA="${SUID_BIN_LOC}"

USAGE="Usage: $0 [pdf] JOBID"

if [ "$1" == "" ]; then
  echo $USAGE
elif [ "$1" == "pdf" ]; then
  PFITTMP=$(mktemp -d)
  if [ "$2" == "" ]; then
    echo $USAGE
    exit 0
  fi
  JOBID="$2"

  JSONPATH=${PFITTMP}/${JOBID}.pdf.json
  $BDATA -t pdf $JOBID > ${JSONPATH}

  python3 $BPDF $JSONPATH $PFITTMP ./
else
  JOBID="$1"

  JSON="$($BDATA -t text $JOBID)"

  if [ $? -eq 0 ]; then
    echo "$JSON" | $BTEXT
  else
    echo $JSON
  fi
fi
