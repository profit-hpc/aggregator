# this script fetches everything related to GPU of the nodes
# and returns parsed dictionary

import math
from collections import defaultdict
import conf.config as conf
from . import common
from . import metrics

DB_INTERVAL = conf.METRIC_INTERVAL
DB_GPU_MES = conf.measurements["gpu_node"]
DB_GPU_PROC_MES = conf.measurements["gpu_proc"]

def get_query_used_gpus(job_id):
    mes = DB_GPU_PROC_MES
    random_field = metrics.metrics_gpu["gpu_cpu_usage_max"]["dbname"]

    query = (
        'SELECT last({field}) as "{field}" FROM "{m}" '
        'WHERE ("JOBID" = \'{jobid}\') '
        'GROUP BY "host", "bus";'
    ).format(m = mes, jobid = job_id, field = random_field)

    return query

def get_query_proc_count_per_device_interval_seq(metric, t_start, t_end, delta, job_id):
    mes = DB_GPU_PROC_MES

    query = (
        'SELECT count(distinct("pid")) as "{met}" FROM "{m}" '
        'WHERE ("JOBID" = \'{jobid}\') '
        'AND time >= {ts:d}s AND time <= {te:d}s '
        'GROUP BY "host", "bus", time({i:d}s);'
    ).format(m = mes, jobid = job_id, i = delta, met = metric,
        ts = t_start, te = t_end
    )

    return query

def get_query_proc_count_per_device_total(metric, job_id):
    mes = DB_GPU_PROC_MES

    query = (
        'SELECT count(distinct("pid")) as "{met}" FROM "{m}" '
        'WHERE ("JOBID" = \'{jobid}\') '
        'GROUP BY "host", "bus";'
    ).format(m = mes, jobid = job_id, met = metric)

    return query

def format_gpu_node_list(gpus, gpu_bind = True):
    final_cond = ""

    if gpu_bind is False:
        final_cond = ' OR '.join("host={:s}".format(n) for n in gpus)
    else:
        nodes_fmtd = []
        for node, buses in gpus.items():
            buses_fmtd = ' OR '.join("bus='{:s}'".format(b) for b in buses)
            nodes_fmtd.append("host='{node}' AND ({buses})".format(node = node, buses = buses_fmtd))

        final_cond = ' OR '.join("({:s})".format(n) for n in nodes_fmtd)

    return final_cond

def get_query_gpu_last(metric, metric_db, t_start, t_end, gpus):
    node_cond = format_gpu_node_list(gpus)
    mes = DB_GPU_MES

    query = (
        'SELECT last("{metdb}") as "{met}" FROM "{mes}" '
        'WHERE ({nodes}) '
        'AND time >= {ts:d}s AND time <= {te:d}s '
        'GROUP BY "host", "bus";'
    ).format(met = metric, metdb = metric_db, nodes = node_cond, mes = mes,
        ts = t_start, te = t_end
    )

    return query

def get_query_aggr_per_device(metric, metric_db, t_start, t_end, gpus, aggr):
    aggr_fmtd = '{:s}("{:s}") as "{:s}"'.format(aggr, metric_db, metric)
    node_cond = format_gpu_node_list(gpus)
    mes = DB_GPU_MES

    query = (
        'SELECT {aggr:s} FROM "{mes:s}" '
        'WHERE ({nodes}) '
        'AND time >= {ts:d}s AND time <= {te:d}s '
        'GROUP BY "host", "bus";'
    ).format(aggr = aggr_fmtd, mes = mes,
        nodes = node_cond, ts = t_start, te = t_end
    )

    return query

def get_query_aggr_per_device_seq(metric, metric_db, t_start, t_end, delta, gpus, aggr):
    aggr_fmtd = '{:s}("{:s}") as "{:s}"'.format(aggr, metric_db, metric)
    node_cond = format_gpu_node_list(gpus)
    mes = DB_GPU_MES

    query = (
        'SELECT {aggr} FROM "{mes}" '
        'WHERE ({nodes}) '
        'AND time >= {ts:d}s AND time <= {te:d}s '
        'GROUP BY "host", "bus", time({delta}s);'
    ).format(aggr = aggr_fmtd, mes = mes, nodes = node_cond,
        ts = t_start, te = t_end, delta = delta
    )

    return query

def get_query_proc_aggr_per_bus_for_job(metric, metric_db, t_start, t_end, delta, job_id, aggr_in, aggr_out):
    mes = DB_GPU_PROC_MES

    query = (
        'SELECT {agr_out}("{met}") as "{met}" FROM ('
        'SELECT {agr_in}("{mdb}") as "{met}" FROM "{mes}" '
        'WHERE "JOBID" = \'{job}\' '
        'AND time >= {ts:d}s AND time <= {te:d}s '
        'GROUP BY "host", "bus", time({dbdelta}s)) '
        'WHERE time >= {ts:d}s AND time <= {te:d}s '
        'GROUP BY "host", "bus";'
    ).format(agr_out = aggr_out, agr_in = aggr_in, mes = mes, job = job_id,
        ts = t_start, te = t_end, delta = delta, met = metric,
        dbdelta = DB_INTERVAL, mdb = metric_db)

    return query

def get_query_proc_aggr_per_bus_for_job_seq(metric, metric_db, t_start, t_end, delta, job_id, aggr):
    aggr = '{:s}("{:s}") as "{:s}"'.format(aggr, metric_db, metric)
    mes = DB_GPU_PROC_MES

    query = (
        'SELECT mean("{met}") as "{met}" FROM ('
        'SELECT {agr} FROM "{mes}" '
        'WHERE "JOBID" = \'{job}\' '
        'AND time >= {ts:d}s AND time <= {te:d}s '
        'GROUP BY "host", "bus", time({dbdelta}s)) '
        'WHERE time >= {ts:d}s AND time <= {te:d}s '
        'GROUP BY "host", "bus", time({delta}s);'
    ).format(agr = aggr, mes = mes, job = job_id,
        ts = t_start, te = t_end, delta = delta, met = metric,
        dbdelta = DB_INTERVAL)

    return query

def parse_gpu_used(data):
    nodes = defaultdict(dict)

    if "series" not in data[0]:
        if "statement_id" in data[0]:
            if conf.DEBUG:
                print("Could not execute query to get used GPUs", queries[sid])
            return {}
        else:
            raise RuntimeError("Cannot parse the output of GPU count query", data[i])

    for s in range(0, len(data[0]['series'])):
        bus = data[0]['series'][s]['tags']['bus']
        host = data[0]['series'][s]['tags']['host']
        if host in nodes:
            nodes[host].append(bus)
        else:
            nodes[host] = [bus]

    return nodes

def get_gpu_used_nodes(job_id):
    query = get_query_used_gpus(job_id)

    gpu_used_raw = common.fetch_data(query)

    nodes = parse_gpu_used(gpu_used_raw)

    return nodes

def get_gpu_queries(job_id, t_start, t_end, gpus, delta):
    queries = ""

    for m, par in metrics.metrics_gpu.items():
        if par["query"] is metrics.QType.LAST_OF_DEVICE:
            q = get_query_gpu_last(m, par["dbname"], t_start, t_end, gpus)
        elif par["query"] is metrics.QType.MAX_PER_DEVICE:
            q = get_query_aggr_per_device(m, par["dbname"], t_start, t_end, gpus, "max")
        elif par["query"] is metrics.QType.AVG_PER_DEVICE:
            q = get_query_aggr_per_device(m, par["dbname"], t_start, t_end, gpus, "mean")
        elif par["query"] is metrics.QType.AVG_PER_DEVICE_INT:
            q = get_query_aggr_per_device_seq(m, par["dbname"], t_start, t_end, delta, gpus, "mean")
        elif par["query"] is metrics.QType.MAX_SUM_PER_INT:
            q = get_query_proc_aggr_per_bus_for_job(m, par["dbname"], t_start, t_end, delta, job_id, "sum", "max")
        elif par["query"] is metrics.QType.PROC_SUM_PER_DEVICE_INT:
            q = get_query_proc_aggr_per_bus_for_job_seq(m, par["dbname"], t_start, t_end, delta, job_id, "sum")
        elif par["query"] is metrics.QType.CUSTOM:
            if m == "gpu_cpu_proc_count_total":
                q = get_query_proc_count_per_device_total(m, job_id)
            elif m == "gpu_cpu_proc_count_sum_seq":
                q = get_query_proc_count_per_device_interval_seq(m, t_start, t_end, delta, job_id)
        else:
            raise ValueError('Unknown query type:', par["query"])

        queries += q

    return queries

def format_seq_val(metric, val):
    if metric not in metrics.metrics_gpu:
        return val

    fmtd = common.format_value(metrics.metrics_gpu[metric]["type"], val)

    return fmtd

def parse_gpu_response(data, queries):
    nodes = {}

    queries = queries.split(";")

    for i in range(0, len(data)):
        if "series" not in data[i]:
            if "statement_id" in data[i]:
                sid = data[i]["statement_id"]
                if len(queries) > sid:
                    if conf.DEBUG:
                        print("The query was not executed", queries[sid])
                else:
                    if conf.DEBUG:
                        print("Some queries were not executed", data[i])
            else:
                raise RuntimeError("Cannot parse the output", data[i])

            continue

        for s in range(0, len(data[i]['series'])):
            series = data[i]['series'][s]

            host =series['tags']['host']
            bus = series['tags']['bus']
            metric = series['columns'][1]

            if len(series['values']) == 1:
                if host not in nodes: nodes[host] = defaultdict(dict)
                nodes[host][bus][metric] = series['values'][0][1]
            elif len(series['values']) > 1:
                if host not in nodes: nodes[host] = defaultdict(dict)
                nodes[host][bus][metric] = []
                for v in range(0, len(series['values'])):
                    nodes[host][bus][metric].append(
                        format_seq_val(metric, series['values'][v][1])
                    )

    return nodes

def format_data(gpuinfo_init):
    gpuinfo = {}

    for node, busses in gpuinfo_init.items():
        for bus, vars in busses.items():
            for name, par in metrics.metrics_gpu.items():
                raw_value = vars.get(name, None)

                if isinstance(raw_value, list):
                    gpuinfo[node][bus][name] = raw_value
                else:
                    value = common.format_metric(metrics.metrics_gpu, name, raw_value)
                    if node not in gpuinfo: gpuinfo[node] = defaultdict(dict)
                    gpuinfo[node][bus][name] = value

    return gpuinfo


def get_gpu_data(job_id, t_start, t_end, node_ids):
    delta = math.ceil((t_end - t_start) / conf.SEQ_MAX_POINTS)
    delta = max(delta, conf.METRIC_INTERVAL)

    used_gpus = get_gpu_used_nodes(job_id)

    if len(used_gpus) == 0:
        return {}

    query = get_gpu_queries(job_id, t_start, t_end, used_gpus, delta)

    gpudata_raw = common.fetch_data(query)

    gpudata_parsed = parse_gpu_response(gpudata_raw, query)

    gpudata = format_data(gpudata_parsed)

    return gpudata
