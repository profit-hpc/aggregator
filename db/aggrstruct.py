class SeqVals:
    delta = None
    seq = []
    def __init__(self, delta=None, seq=None):
        self.delta = delta
        self.seq = seq

class ProcData:
    cpu_time_user = None
    cpu_time_system = None
    cpu_time_idle = None
    cpu_time_iowait = None
    write_bytes = None
    write_count = None
    read_bytes = None
    read_count = None
    mem_rss_max = None
    mem_rss_avg = None
    mem_swap_max = None
    mem_vms = None
    cpu_usage = None
    seq_cpu_usage = None
    seq_mem_rss_sum = None
    seq_write_sum = None
    seq_read_sum = None

    def __init__(self):
        self.seq_cpu_usage = SeqVals()
        self.seq_mem_rss_sum = SeqVals()
        self.seq_write_sum = SeqVals()
        self.seq_read_sum = SeqVals()

class GPUData:
    bus = None
    power_limit = None
    mem = None
    mem_max = None
    temp_max = None
    power_max = None
    usage_max = None
    usage_avg = None
    cpu_usage_max = None
    cpu_mem_rss_max = None
    cpu_proc_total = None

    def __init__(self):
        self.seq_mem_avg = SeqVals()
        self.seq_usage_avg = SeqVals()
        self.seq_temp_avg = SeqVals()
        self.seq_power_avg = SeqVals()
        self.seq_cpu_usage_sum = SeqVals()
        self.seq_cpu_mem_rss_sum = SeqVals()
        self.seq_cpu_proc_count = SeqVals()

class AllocCUData:
    node_id = None
    cu_count = None
    def __init__(self, node=None, cu=None):
        self.node_id = node
        self.cu_count = cu

class JobData:
    id = None
    user_name = None
    used_queue = None
    submit_time = None
    start_time = None
    end_time = None
    run_time = None
    requested_time = None
    requested_cu = None
    num_nodes = None
    exit_code = None
    alloc_cu = [AllocCUData()]

class NodeData:
    name = None
    cpu_model = None
    sockets = None
    cores_per_socket = None
    virt_thr_core = None
    phys_thr_core = None
    main_mem = None
    seq_cpu_usage = None
    seq_load_avg = None
    seq_load_max = None
    seq_ib_rcv_max = None
    seq_ib_xmit_max = None
    proc = None
    alloc_cu = None
    alloc_mem = None
    ib_rcv_max = None
    ib_xmit_max = None
    beegfs_read_sum = None
    beegfs_write_sum = None

    def __init__(self):
        self.proc = ProcData()
        self.gpus = [GPUData()]
        self.seq_load_avg = SeqVals()
        self.seq_load_max = SeqVals()
        self.seq_cpu_usage = SeqVals()
        self.seq_ib_rcv_max = SeqVals()
        self.seq_ib_xmit_max = SeqVals()
        self.seq_beegfs_read_sum = SeqVals()
        self.seq_beegfs_write_sum = SeqVals()


class Aggregator:
    job = JobData()
    nodes = [NodeData()]
