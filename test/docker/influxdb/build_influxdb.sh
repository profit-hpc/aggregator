#! /bin/bash

# root path of repo
ROOTOFREP=`git rev-parse --show-toplevel`

if [ "$1" == "" ]; then
  ITAG="pfit-influxdb"
else
  ITAG="$1"
fi

cd $ROOTOFREP/test/docker/influxdb

docker build -t $ITAG -f $ROOTOFREP/test/docker/influxdb/Dockerfile .
