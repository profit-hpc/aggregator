import matplotlib.pyplot as plt
import numpy as np

XLIM = 300
YLIM = 100
x_const = 8

tx = np.arange(1.0, XLIM)

plt.axis([1, XLIM, 0, YLIM])
plt.ylabel('CPU usage (U, %)')
plt.xlabel('Requested cores (R)')
plt.grid(True, which="both", linestyle='--')

# lines
y_is_50 = tx*0 + 50
y_is_80 = tx*0 + 80
x_is_const = [x_const, x_const], [0, YLIM]

# plot the lines
plt.plot(tx, y_is_50, label="U=50%")
plt.plot(tx, y_is_80, label="U=80%")
plt.plot(*x_is_const, label="R=8")

# highlight the area
plt.fill_between(tx, y_is_50, where=tx <= x_const, color='red', alpha=0.3)
plt.fill_between(tx, y_is_80, where=tx >= x_const, color='red', alpha=0.3)

plt.legend(loc='lower right')

plt.savefig('../../img/cpu_usage_total_max.svg')
plt.show()
