#! /usr/bin/env python3
import sys
import argparse
import json
import os

DEBUG = False

def load_json(fname):
    infile = os.path.abspath(fname)
    doc = {}
    with open(infile, 'r') as f:
        doc = json.load(f)
    return doc

def parse_tags(tags_raw):
    tags = []
    for t in tags_raw["results"][0]["series"][0]["values"]:
        tags.append(t[0])

    if DEBUG:
        print("Tags in file: ", tags)

    return tags

def parse_values(val_raw):
    cols = val_raw["results"][0]["series"][0]["columns"]

    vals = []
    for i in range(0, len(val_raw["results"])):
        vals += val_raw["results"][i]["series"][0]["values"]

    if DEBUG:
        print("Cols in file:", cols)
        print("Vals in file:", vals[0])

    return cols, vals

def parse_metric(tags_raw, val_raw):
    tm = tags_raw["results"][0]["series"][0]["name"]
    vm = val_raw["results"][0]["series"][0]["name"]

    if tm != vm:
        raise ValueError("Metric names in vals and tags don't matchs")

    return tm


def sep_columns(tags, cols, val):
    vtags = {}
    vcols = {}

    for i, c in enumerate(cols):
        v = val[i]
        if v is None: continue

        if c in tags:
            vtags[c] = v
        else:
            vcols[c] = v

    return vtags, vcols


def format_line(metric, time, tags, vals):
    if DEBUG:
        print("metric:", metric)
        print("time:", time)
        print("tags:", tags)
        print("vals:", vals)
    out_tags = ",".join("{:s}={:s}".format(t,v) for t,v in tags.items())

    out_vals = ",".join("{:s}={:}".format(
        c,"\"{:}\"".format(v) if type(v) == str else v) for c,v in vals.items())

    out = "{:s},{:s} {:s} {:d}".format(metric, out_tags, out_vals, time)

    return out

def output_line_protocol(metric, tags, cols, vals):
    timeidx = cols.index("time")

    for k, v in enumerate(vals):
        if DEBUG and k > 2:
            break

        time = v[timeidx]
        v[timeidx] = None

        vtags, vcols = sep_columns(tags, cols, v)

        o = format_line(metric, time, vtags, vcols)

        print(o)

def main():
    global DEBUG

    parser = argparse.ArgumentParser(description="""
        Formats JSON output of InfluxDB to line protocol
    """)

    parser.add_argument("-v", "--values", required=True,
                        help="filename of JSON file with values")
    parser.add_argument("-t", "--tags", required=True,
                        help="filename of JSON file with tag keys")
    parser.add_argument("-d", "--debug", action="store_true",
                        help="debugs the tool. Prints only first 3 points")

    args = parser.parse_args()

    values_raw = load_json(args.values)
    tags_raw = load_json(args.tags)

    DEBUG = args.debug

    metric = parse_metric(tags_raw, values_raw)

    tags = parse_tags(tags_raw)
    columns, values = parse_values(values_raw)

    output_line_protocol(metric, tags, columns, values)

if __name__ == "__main__":
    main()
