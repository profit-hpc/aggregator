# Attributes

**Definition**: attribute is a property of the job with predefined values.

Here are listed the attributes used for making rules.

**Common parameters of the attributes:**
- If there is an error during calculation of the attribute, the return value is `ERR`

## req_walltime

The attribute for the requested walltime.

**Values**:

`HIGH` - The requested walltime is not in the tolerable range

`NORM` - The requested walltime is in the tolerable range

**Calculation**:

`R` - Requested walltime (hours). `U` - Used walltime (hours).

`HIGH` = `R > 4` & `U < R / 2` & `U < R - 6`

`NORM` = ! `HIGH`

**Extra**:

![req_walltime](img/req_walltime.svg)

*The `HIGH` values are highlighted*

## node_cpu_usage_max

The attribute for the maximum `cpu_usage` among the nodes.

The optimal CPU usages depends on the amount of cores requested. When the number of requested cores is not very high, `cpu_usage` should not be near 100%, since the program might be not CPU bound (as soon as we can check it we can make more elaborate attribute). But when the amount of requested CPUs get higher, they should be used more efficiently.

**Values**:
`HIGH` - The maximum `cpu_usage` is greater than 100%

`NORM` - The maximum `cpu_usage` is in the tolerable range

`LOW` - The maximum `cpu_usage` is lower than tolerable value

`ZERO` - The maximum `cpu_usage` is 0

**Calculation**:

`U` - the sum of `cpu_usage` of all processes on a single node, divided by the number of requested cores.

`R` - requested amount of cores on a single node.

`HIGH` = if such node exists, that `U > 100`

`NORM` = !`HIGH` & if such node exists, that `U > 50` & `R <= 8` | `U > 80` & `R > 8` holds

`LOW` = !`NORM` & if such node exists, that `U < 50` & `R <= 8` | `U < 80` & `R > 8` holds

`ZERO` = !`LOW` & !`NORM` & !`HIGH`

![node_cpu_usage_max](img/node_cpu_usage_max.svg)

*The corresponding values are highlighted*

`LOW` is red. `NORM` is green and `HIGH` is orange.

## node_cpu_usage_min

The attribute for the minimum `cpu_usage` among the nodes.

Similar to the attribute `node_cpu_usage_max` but indicates the minimum of `cpu_usage` on the node.

**Values**:
`HIGH` - The minimum `cpu_usage` is greater than 100%

`NORM` - The minimum `cpu_usage` is in the tolerable range

`LOW` - The minimum `cpu_usage` is lower than tolerable value

`ZERO` - The minimum `cpu_usage` is 0

**Calculation**:

`U` - the sum of `cpu_usage` of all processes on a single node, divided by the number of requested cores.

`R` - requested amount of cores on a single node.

`ZERO` = if such node exists, that `U = 0`

`LOW` = !`ZERO` & if such node exists, that `U < 50` & `R <= 8` | `U < 80` & `R > 8` holds

`NORM` = !`ZERO` & !`LOW` & such node exists, that `U > 50` & `R <= 8` | `U > 80` & `R > 8` holds

`HIGH` = !`ZERO` & !`LOW` & !`NORM`

![node_cpu_usage_max](img/node_cpu_usage_max.svg)

*The corresponding values are highlighted*

`LOW` is red. `NORM` is green and `HIGH` is orange.

## cpu_usage_total_max

The attribute for the sum of maximum `cpu_usage` per node.

The attribute is very similar to **node_cpu_usage_max** but instead of global maximum `cpu_usage` it compares the total sum of maximum `cpu_usage` of the nodes. The calculations are similar too.

**Values**:

`NORM` - The maximum `cpu_usage` is in the tolerable range

`LOW` - The maximum `cpu_usage` is lower than tolerable value

**Calculation**:

`U` - the global sum of maximum sums of `cpu_usage` of all processes on the nodes, divided by the number of requested cores.

`R` - requested amount of cores on a single node.

`LOW` = `U < 50` & `R <= 8` | `U < 80` & `R > 8`

![cpu_usage_total_max](img/cpu_usage_total_max.svg)

*The `LOW` values are highlighted*

## job_nodes_amount

The attribute for the amount of nodes allocated for the job.

It simply divides the jobs into 2 categories: running on a single node and running on multiple nodes.

**Values**:

`ONE` - if the number of nodes equals 1

`MULT` - if the number of nodes is greater than 1

## mem_usage_total

The attribute for the ratio between sum of maximum memory usage per node and total requested memory.

**Values**:

`HIGH` - The maximum memory usage is in the tolerable range

`LOW` - The maximum memory usage is lower than the tolerable bound

**Calculation**:

`U` - the ratio of the global sum of maximum sums of memory usage (RSS) of all processes on the nodes to requested memory `R`.

`R` - the sum of requested amount of memory on every node in GiB.

`LOW` = `U < 30` & `R > 32`

`HIGH` = !`LOW`

![cpu_usage_total_max](img/mem_usage_total.svg)

## overloaded_node_exists

It is `True` if the node has a high load during the interval `D`.

**Values**:

`True` - if such node exists

`False` - otherwise

**Calculation**:

If during the interval `D` (300 seconds) consequent measurements of `load1` on the node exceeds the amount of cores the node has, then the value is `True`,  `False` otherwise.

## mem_swap_used

It is `True` if there is a node where swap memory was used.

**Values**:

`True` - if swap was used on one of the nodes

`False` - otherwise

**Calculation**:

For every node check if `mem_swap_max` value is non zero. If it is non zero for any node, then set the attribute to `True`, otherwise `False`.

## gpu_job

It is `True` if the job used at least one GPU

**Values**:

`True` - if GPU was used

`False` - otherwise

**Calculation**:

For every node check if GPU was used. If used then set the attribute to `True`, otherwise `False`.

## gpu_usage_max

This attribute indicates the maximum of GPU usage among all GPUs which were running processes of the job.

**Values**:
`HIGH` - The maximum GPU usage is almost 100%

`NORM` - The maximum GPU usage is in the tolerable range

`LOW` - The maximum GPU usage is lower than tolerable value

`ZERO` - The maximum GPU usage is 0

**Calculation**:

`U` - the GPU usage of particular GPU (max 100%).

`ZERO` = if such GPU exists, that `U <= 0.5`

`LOW` = !`ZERO` & if such GPU exists, that `U < 50` holds

`NORM` = !`ZERO` & !`LOW` & such node exists, that `U < 90` holds

`HIGH` = !`ZERO` & !`LOW` & !`NORM`

## gpu_usage_min

This attribute indicates the minimum of GPU usage among all GPUs which were running processes of the job.

Similar to `gpu_usage_max` but indicates the minimum GPU usage.

**Values**:
`HIGH` - The minimum GPU usage is almost 100%

`NORM` - The minimum GPU usage is in the tolerable range

`LOW` - The minimum GPU usage is lower than tolerable value

`ZERO` - The minimum GPU usage is 0

**Calculation**:

`U` - the GPU usage of particular GPU (max 100%).

`ZERO` = if such GPU exists, that `U <= 0.5`

`LOW` = !`ZERO` & if such GPU exists, that `U < 50` holds

`NORM` = !`ZERO` & !`LOW` & such node exists, that `U < 90` holds

`HIGH` = !`ZERO` & !`LOW` & !`NORM`

## gpus_amount

The attribute for the amount of gpus used in the runtime of the job.

It simply divides the jobs into 2 categories: using a single GPU and using multiple GPUs.

**Values**:

`ONE` - if the number of GPUs equals 1

`MULT` - if the number of GPUs is greater than 1

## gpus_overcrowded_exist


It is `True` if any GPU has multiple processes running on it during the interval `D`.

**Values**:

`True` - if such GPU exists

`False` - otherwise

**Calculation**:

If during the interval `D` (1800 seconds) consequent measurements of number of processes on the GPU exceeds 1, then the value is `True`, `False` otherwise.
