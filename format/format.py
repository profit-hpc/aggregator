# this script contains the main function format to format the data ready to
# output
from db.aggrstruct import *
from .formats_json import OutputSchemes

def get_value(agg, path, **ids):
    if path == "":
        return agg

    classes = path.split(".")

    recvar = agg
    for c in classes:
        if not hasattr(recvar, c):
            return None
            raise ValueError("scheme: {} from {} was not found".format(c, path))

        recvar = getattr(recvar, c)

        if c in ids:
            recvar = recvar[ids[c]]

    return recvar

def build_path(prefix, suffix):
    path = suffix
    if prefix != "":
        path = "{}.{}".format(prefix, suffix)

    return path

def fill_scheme(agg, scheme, path, **ids):
    # fill all standard fields via simple copy
    result = {}

    for fkey, fval in scheme.items():
        if fval is None:
            continue

        if type(fval) is dict:
            if "scheme" in fval and "path" in fval:
                fres = format_scheme(agg, fval, path, **ids)
            else:
                fres = fill_scheme(agg, fval, path, **ids)

        if type(fval) is str:
            fres = get_value(agg, build_path(path, fval), **ids)

        result[fkey] = fres

    return result

def format_scheme(agg, sstruc, path="", **ids):
    if sstruc["type"] == "dict":
        result = fill_scheme(agg, sstruc["scheme"], build_path(path, sstruc["path"]), **ids)

    elif sstruc["type"] == "list":
        result = []
        for fkey, _ in enumerate(get_value(agg, build_path(path, sstruc["path"]), **ids)):
            ids[sstruc["path"]] = fkey
            fres = fill_scheme(agg, sstruc["scheme"], build_path(path, sstruc["path"]), **ids)
            result.append(fres)

    elif sstruc["type"] == "seq":
        seq_data = get_value(agg, build_path(path, sstruc["path"]), **ids)
        result = {
            "delta": seq_data.delta,
            "data": seq_data.seq,
        }

    if not result: result = None
    
    return result


def format_text(agg):
    scheme = OutputSchemes.text
    result = {}

    result = format_scheme(agg, scheme)

    result = format_acsii_nonstd(agg, result)

    return result

def format_pdf(agg):
    scheme = OutputSchemes.pdf
    result = {}

    result = format_scheme(agg, scheme)

    result = format_acsii_nonstd(agg, result)

    return result

def format(agg, type="text"):
    if not isinstance(agg, Aggregator):
        raise RuntimeError("No aggregator in the format method")

    assert type in ["text", "pdf"]

    if type == "text":
        formatted = format_text(agg)
    elif type == "pdf":
        formatted = format_pdf(agg)

    return formatted

def format_acsii_nonstd(agg, result):
    # Custom formatters


    return result
