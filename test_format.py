#! /usr/bin/env python3

from pprint import pprint

from db import aggregator
from db import aggrstruct as data
from format import format


agg = data.Aggregator()

# fill some random data
agg.job = data.JobData()
for f in dir(agg.job):
    if not f.startswith('__') and not callable(getattr(agg.job,f)):
        setattr(agg.job, f, "job_field_" + f + "_value")
agg.job.alloc_cu = [data.AllocCUData("node_0_name_value", 1),
    data.AllocCUData("node_1_name_value", 4)
]
agg.nodes = [data.NodeData(), data.NodeData()]

for i, _ in enumerate(agg.nodes):
    agg.nodes[i].proc = data.ProcData()

    cls = agg.nodes[i].proc
    for f in dir(cls):
        if not f.startswith('__') and not callable(getattr(cls,f)) and getattr(cls, f) is None:
            setattr(agg.nodes[i].proc, f, "proc_" + str(i) + "_" + str(f) + "_value")

    cls = agg.nodes[i]
    for f in dir(cls):
        if not f.startswith('__') and not callable(getattr(cls,f)) and getattr(cls, f) is None:
            setattr(agg.nodes[i], f, "node_" + str(i) + "_" + str(f) + "_value")

# format and output JSON
pprint(format.format_text(agg))
