import matplotlib.pyplot as plt
import numpy as np

XLIM = 512
YLIM = 100
x_const = 32

tx = np.arange(1.0, XLIM)

plt.axis([1, XLIM, 0, YLIM])
plt.ylabel('Memory usage (U, %)')
plt.xlabel('Requested memory (R)GiB')
plt.grid(True, which="both", linestyle='--')

# lines
y_is_30 = tx*0 + 30
x_is_const = [x_const, x_const], [0, YLIM]

# plot the lines
plt.plot(tx, y_is_30, label="U=30%")
plt.plot(*x_is_const, label="R=32")

# highlight the area
plt.fill_between(tx, y_is_30, where=tx >= x_const, color='red', alpha=0.3)

plt.legend(loc='lower right')

plt.savefig('../../img/mem_usage_total.svg')
plt.show()
